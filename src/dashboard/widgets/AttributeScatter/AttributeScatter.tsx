import React from "react";
import AttributeScatterValues from "./AttributeScatterValues";

import {
  WidgetDefinition,
  AttributeInput,
  AttributeInputDefinition,
  SelectInputDefinition,
  BooleanInputDefinition,
  ColorInputDefinition
} from "../../types";
import { WidgetProps } from "../types";
import { showHideTangoDBName } from "../../DBHelper";


type Inputs = {
  independent: AttributeInputDefinition;
  dependent: AttributeInputDefinition;
  showAttribute: SelectInputDefinition;
  showTangoDB: BooleanInputDefinition;
  xScientificNotation: BooleanInputDefinition;
  yScientificNotation: BooleanInputDefinition;
  xLogarithmicScale: BooleanInputDefinition;
  yLogarithmicScale: BooleanInputDefinition;
  textColor: ColorInputDefinition;
  backgroundColor: ColorInputDefinition;
};

type Props = WidgetProps<Inputs>;

function fullName(attribute: AttributeInput, showAttribute: string, showTangoDB: boolean) {
  let display = "";
  const attributeDevice = showHideTangoDBName(true, showTangoDB, attribute.device)
  if (showAttribute === "Label") display = attribute.label;
  else if (showAttribute === "Name") display = attribute.attribute;
  return `${attributeDevice || "?"}/${display || "?"}`;
}

function AttributeScatter(props: Props) {
  const { mode, inputs, actualWidth, actualHeight } = props;
  const staticMode = mode !== "run";

  const { 
    dependent, 
    independent, 
    showAttribute, 
    showTangoDB, 
    xScientificNotation, 
    yScientificNotation, 
    xLogarithmicScale, 
    yLogarithmicScale, 
    textColor, 
    backgroundColor 
  } = inputs;
  const independentName =
    mode === "library" ? "attribute 1" : fullName(independent, showAttribute, showTangoDB);
  const dependentName =
    mode === "library" ? "attribute 2" : fullName(dependent, showAttribute, showTangoDB);

  const defaultRange = mode !== "run" ? { range: [-5, 5] } : {};

  const layout = {
    font: { family: "Helvetica, Arial, sans-serif", color: textColor },
    paper_bgcolor: backgroundColor,
    plot_bgcolor: backgroundColor,
    margin: {
      l: 45,
      r: 15,
      t: 15,
      b: 35,
    },
    autosize: true,
    hovermode: "closest",
    xaxis: {
      title: independentName,
      titlefont: { size: 12 },
      type: xLogarithmicScale ? "log" : "linear",
      exponentformat: xScientificNotation ? "e" : "none",
      ...defaultRange,
    },
    yaxis: {
      title: dependentName,
      titlefont: { size: 12 },
      type: yLogarithmicScale ? "log" : "linear",
      exponentformat: yScientificNotation ? "e" : "none",
      ...defaultRange,
    },
  };
  return (
    <AttributeScatterValues
      dependent={dependent}
      independent={independent}
      mode={mode}
      layout={layout}
      config={{ staticPlot: staticMode === true }}
      responsive={true}
      style={{
        width: actualWidth,
        height: mode === "library" ? 150 : actualHeight,
      }}
    />
  );
}
const definition: WidgetDefinition<Inputs> = {
  type: "ATTRIBUTE_SCATTER",
  name: "Attribute Scatter",
  defaultWidth: 30,
  defaultHeight: 20,
  inputs: {
    showAttribute: {
      type: "select",
      label: "Attribute display:",
      default: "Label",
      options: [
        {
          name: "Label",
          value: "Label",
        },
        {
          name: "Name",
          value: "Name",
        },
      ],
    },
    independent: {
      label: "Independent Attribute",
      type: "attribute",
      required: true,
      dataFormat: "scalar",
      dataType: "numeric",
    },
    dependent: {
      label: "Dependent Attribute",
      type: "attribute",
      required: true,
      dataFormat: "scalar",
      dataType: "numeric",
    },
    showTangoDB: {
      type: "boolean",
      label: "Show Tango database name",
      default: false,
    },
    xScientificNotation: {
      type: "boolean",
      label: "X-axis Scientific Notation",
      default: false,
    },
    yScientificNotation: {
      type: "boolean",
      label: "Y-axis Scientific Notation",
      default: false,
    },
    xLogarithmicScale: {
      type: "boolean",
      label: "X-axis Logarithmic Scale",
      default: false,
    },
    yLogarithmicScale: {
      type: "boolean",
      label: "Y-axis Logarithmic Scale",
      default: false,
    },
    textColor: {
      label: "Text Color",
      type: "color",
      default: "#000000",
    },
    backgroundColor: {
      label: "Background Color",
      type: "color",
      default: "#ffffff",
    },
  },
};
const AttributeScatterExport = { definition, component: AttributeScatter };
export default AttributeScatterExport;
