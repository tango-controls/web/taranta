import React from "react";
import { act } from "react-dom/test-utils";
import { DeviceInput } from "../../../types";
import { fireEvent, render, waitFor } from "@testing-library/react";
import Enzyme from "enzyme";
import TangoAPI from "../../../../shared/api/tangoAPI";
import { useSelector as useSelectorMock, useDispatch } from "react-redux";
import Adapter from "@cfaester/enzyme-adapter-react-18";
import { getAttributeLastValueFromState as getAttributeLastValueFromStateMock } from "../../../../shared/utils/getLastValueHelper";
import { getAttributeLastTimeStampFromState as getAttributeLastTimeStampFromStateMock } from "../../../../shared/utils/getLastValueHelper";
import MacroButton from "./MacroButton";

interface Inputs {
  door: DeviceInput;
  macroserver: DeviceInput;
  pool: DeviceInput;
  alignButtonRight: boolean;
  outerDivCss: string;
}

const getAttributeLastValueFromState = getAttributeLastValueFromStateMock as jest.Mock;
const getAttributeLastTimeStampFromState = getAttributeLastTimeStampFromStateMock as jest.Mock;
const executeCommand = TangoAPI.executeCommand as jest.Mock;
const fetchAttributes = TangoAPI.fetchAttributesValues as jest.Mock;

jest.mock("react-redux", () => ({
  ...jest.requireActual("react-redux"),
  useSelector: jest.fn(),
  useDispatch: jest.fn(),
}));

jest.mock("../../../../shared/utils/getLastValueHelper", () => ({
  getAttributeLastValueFromState: jest.fn(),
  getAttributeLastTimeStampFromState: jest.fn(),
}));

jest.mock("../../../../shared/api/tangoAPI", () => ({
  executeCommand: jest.fn(),
  fetchAttributesValues: jest.fn(),
}));
window.alert = jest.fn(() => ({}));
const useSelector = useSelectorMock as jest.Mock;
Enzyme.configure({ adapter: new Adapter() });

const { component: MacroButtonComponent } = MacroButton;

describe("AttributeWriterDropdown", () => {
  const initialDoorInput: DeviceInput = {
    alias: "",
    name: "door/test/1",
  };

  const initialMacroServerInput: DeviceInput = {
    alias: "",
    name: "macroserver/test/1",
  };

  const initialPoolDeviceInput: DeviceInput = {
    alias: "",
    name: "pool/test/1",
  };

  const initialInputs: Inputs = {
    door: initialDoorInput,
    macroserver: initialMacroServerInput,
    pool: initialPoolDeviceInput,
    alignButtonRight: false,
    outerDivCss: "",
  };

  const initialProps = {
    t0: 1,
    actualWidth: 32,
    actualHeight: 12,
    inputs: initialInputs,
  };

  let initialMessage = {
    "macroserver/test/1": {
      attributes: {
        macrolist: {
          values: [["macro1", "macro2"]],
        },
      },
    },
    "motor/test/1": {
      attributes: {
        motorlist: {
          values: [["mot01", "mot02"]],
        },
      },
    },
    "door/test/1": {
      attributes: {
        output: {
          values: ["output1", "output2", "output3", "output4"],
          timestamp: [254.654, 345.897, 478.548, 568.123, 687.987],
        },
        state: {
          values: ["RUNNING", "RUNNING"],
        },
      },
    },
  };

  beforeEach(() => {
    const dispatch = jest.fn();
    useSelector.mockImplementation((selectorFn: any) =>
      selectorFn({
        messages: initialMessage,
        ui: {
          mode: "run",
        },
      })
    );
    getAttributeLastValueFromState.mockImplementation((state, name, field) => {
      if (field === "motorlist") return [JSON.stringify({ name: "mot01" })];
      if (field === "macrolist") return ["macro1", "macro2"];
      return "RUNNING";
    });

    getAttributeLastTimeStampFromState.mockImplementation(
      (state, name, field) => {
        if (field === "output") return 254.654;
        return undefined;
      }
    );

    executeCommand.mockReturnValue({
      output: [
        JSON.stringify({
          description: "description  Mock  test",
          parameters: [{ value: "mot01", name: "mot01", type: "Moveable" }],
        }),
      ],
    });

    fetchAttributes.mockReturnValue([{ value: ["error"] }]);
    (useDispatch as jest.Mock).mockReturnValue(dispatch);
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  it("renders an empty widget in edit/run mode without crashing", () => {
    getAttributeLastValueFromState.mockImplementation((state, name, field) => {
      if (field === "motorlist") return [JSON.stringify({ name: "mot01" })];
      if (field === "macrolist") return ["macro1", "macro2"];
      return "ON";
    });

    const testInput = initialInputs;

    const { container: elementInEdit } = render(
      <MacroButtonComponent mode="edit" id={42} {...initialProps} />
    );

    expect(elementInEdit.innerHTML).toContain("door/test/1");
    expect(elementInEdit.innerHTML).toContain(
      "Door output will be displayed here"
    );

    const { container: elementInRun } = render(
      <MacroButtonComponent mode="run" id={42} {...initialProps} />
    );
    expect(elementInRun.innerHTML).toContain(
      "Door output will be displayed here"
    );
    expect(elementInRun.innerHTML).toContain("Run");
    expect(elementInRun.innerHTML).toContain("Select Macro");
    const items = elementInEdit.getElementsByClassName("macro-list-dropdown");
    expect(items).toHaveLength(1);
    expect(elementInRun.innerHTML).toContain(
      "Door output will be displayed here"
    );
  }); //renders an empty widget in edit/run mode without crashing

  it("it displays selects with good options", async () => {
    const element = render(
      <MacroButtonComponent mode="run" id={42} {...initialProps} />
    );

    let { container: elementInRun } = element;

    expect(elementInRun.innerHTML).toContain("Run");
    expect(elementInRun.innerHTML).toContain("Select Macro");

    const selectElement = elementInRun.querySelectorAll(
      "#macroListSelect"
    )[0] as HTMLInputElement;

    await waitFor(() => {
      fireEvent.change(selectElement, { target: { value: "macro1" } });
    });

    expect(await selectElement.value).toBe("macro1");

    const selectMovalbleElement = elementInRun.querySelectorAll(
      "#movableMacroSelect"
    )[0] as HTMLInputElement;

    await waitFor(() => {
      fireEvent.change(selectMovalbleElement, { target: { value: "mot01" } });
    });

    expect(await selectMovalbleElement.value).toBe("mot01");

    const runButton = elementInRun.querySelectorAll(
      "#executeButton"
    )[0] as HTMLInputElement;

    act(() => {
      fireEvent.click(runButton);
    });
  });

  it("sets the macro attributes in the readonly input field", async () => {
    const element = render(
      <MacroButtonComponent mode="run" id={42} {...initialProps} />
    );

    // Select macro
    let { container: elementInRun } = element;
    const selectElement = elementInRun.querySelectorAll(
      "#macroListSelect"
    )[0] as HTMLInputElement;
    await waitFor(() => {
      fireEvent.change(selectElement, { target: { value: "macro1" } });
    });

    // Select motor
    const selectMovalbleElement = elementInRun.querySelectorAll(
      "#movableMacroSelect"
    )[0] as HTMLInputElement;
    await waitFor(() => {
      fireEvent.change(selectMovalbleElement, { target: { value: "mot01" } });
    });

    // Check the readonly input field
    const readonlyInput = elementInRun.querySelectorAll(
      "[data-ui-id='macro-args']"
    )[0] as HTMLInputElement;
    expect(readonlyInput.value).toBe("mot01");
  });

  it("display output values when state is RUNNING", async () => {
    jest.useFakeTimers();

    const element = render(
      <MacroButtonComponent mode="run" id={42} {...initialProps} />
    );

    let { container: elementInRun } = element;

    const selectElement = elementInRun.querySelectorAll(
      "#macroListSelect"
    )[0] as HTMLInputElement;

    await waitFor(() => {
      fireEvent.change(selectElement, { target: { value: "macro1" } });
    });

    const runButton = elementInRun.querySelectorAll(
      "#executeButton"
    )[0] as HTMLInputElement;

    act(() => {
      fireEvent.click(runButton);
      jest.advanceTimersByTime(1000);
    });
    await waitFor(() => {
      expect(elementInRun.innerHTML).toContain("output2");
      expect(elementInRun.innerHTML).toContain("output3");
      expect(elementInRun.innerHTML).toContain("output4");
    });
  });

  it("display empty output value when timestamp and values in message is empty", async () => {
    jest.useFakeTimers();
    initialMessage["door/test/1"].attributes.output.values = [];
    initialMessage["door/test/1"].attributes.output.timestamp = [];

    const element = render(
      <MacroButtonComponent mode="run" id={42} {...initialProps} />
    );

    let { container: elementInRun } = element;

    const selectElement = elementInRun.querySelectorAll(
      "#macroListSelect"
    )[0] as HTMLInputElement;

    await waitFor(() => {
      fireEvent.change(selectElement, { target: { value: "macro1" } });
    });

    const runButton = elementInRun.querySelectorAll(
      "#executeButton"
    )[0] as HTMLInputElement;

    act(() => {
      fireEvent.click(runButton);
      jest.advanceTimersByTime(1000);
    });

    await waitFor(() => {
      expect(elementInRun.innerHTML).toContain(
        "Door output will be displayed here"
      );
    });
  });

  it("display all values from state when timestamp is 0", async () => {
    jest.useFakeTimers();
    initialMessage["door/test/1"].attributes.output.values = [
      "output5",
      "output6",
      "output7",
    ];
    initialMessage["door/test/1"].attributes.output.timestamp = [
      123.456,
      456.265,
      896.147,
    ];

    getAttributeLastTimeStampFromState.mockImplementation(
      (state, name, field) => {
        if (field === "output") return 0;
        return undefined;
      }
    );

    const element = render(
      <MacroButtonComponent mode="run" id={42} {...initialProps} />
    );

    let { container: elementInRun } = element;

    const selectElement = elementInRun.querySelectorAll(
      "#macroListSelect"
    )[0] as HTMLInputElement;

    await waitFor(() => {
      fireEvent.change(selectElement, { target: { value: "macro1" } });
    });

    const runButton = elementInRun.querySelectorAll(
      "#executeButton"
    )[0] as HTMLInputElement;

    act(() => {
      fireEvent.click(runButton);
      jest.advanceTimersByTime(1000);
    });

    await waitFor(() => {
      expect(elementInRun.innerHTML).toContain("output5");
      expect(elementInRun.innerHTML).toContain("output6");
      expect(elementInRun.innerHTML).toContain("output7");
    });
  });
});
