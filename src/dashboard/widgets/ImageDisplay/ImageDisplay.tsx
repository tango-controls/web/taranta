import React, { Component, CSSProperties } from "react";

import { WidgetProps } from "../types";
import {
  WidgetDefinition,
  BooleanInputDefinition,
  AttributeInputDefinition,
  SelectInputDefinition,
  StringInputDefinition,
  NumberInputDefinition,
  ColorInputDefinition,
  StyleInputDefinition,
} from "../../types";
import { useSelector } from "react-redux";
import { IRootState } from "../../../shared/state/reducers/rootReducer";
import { getAttributeLastValueFromState } from "../../../shared/utils/getLastValueHelper";
import { parseCss } from "../../components/Inspector/StyleSelector";
import { showHideTangoDBName } from "../../DBHelper";
import imgSample from "./image-sample.png";

export interface AttributeComplexInput {
  attribute: AttributeInputDefinition;
}

type Inputs = {
  attribute: AttributeInputDefinition;
  showAttribute: SelectInputDefinition;
  showDevice: BooleanInputDefinition;
  showTangoDB: BooleanInputDefinition;
  scale: StringInputDefinition;
  textColor: ColorInputDefinition;
  backgroundColor: ColorInputDefinition;
  size: NumberInputDefinition;
  font: SelectInputDefinition;
  widgetCss: StyleInputDefinition;
};

type Props = WidgetProps<Inputs>;

class ImageDisplay extends Component<Props> {
  public constructor(props: Props) {
    super(props);
  }

  public render() {
    const { mode, inputs } = this.props;
    const {
      attribute,
      showAttribute,
      showDevice,
      showTangoDB,
      scale,
      textColor,
      size,
      font,
      backgroundColor,
      widgetCss,
    } = inputs;

    const device = attribute.device || "device";
    const CustomCss = parseCss(widgetCss).data;
    let display = "";
    const userScale = (parseFloat(scale) * 100).toString() + "%";
    if (showAttribute === "Label") display = attribute.label;
    else if (showAttribute === "Name") display = attribute.attribute;

    const style: CSSProperties = {
      whiteSpace: "pre-wrap",
      color: textColor,
      fontSize: size + "em",
      backgroundColor: backgroundColor,
      ...CustomCss,
    };

    if (font) style["fontFamily"] = font;

    return (
      <div
        id="ImageDisplay"
        className={mode !== "library" ? "w-100 h-100" : "library-height"}
        style={style}
      >
        <div>
          {showDevice
            ? showHideTangoDBName(showDevice, showTangoDB, device)
            : ""}
          {showDevice && showAttribute && "/"}
          {display}
          {showDevice || showAttribute !== "None"}
        </div>
        <div
          style={{
            width: "100%",
            height: "calc(100% - 25px)",
          }}
        >
          {"run" === mode && (
            <GenerateImage
              device={attribute?.device}
              attribute={attribute?.attribute}
              scale={userScale}
            />
          )}
          {"run" !== mode && (
            <img
              src={imgSample}
              alt="Failed at fetching the device attribute"
              style={{
                width: userScale,
                height: userScale,
              }}
            />
          )}
        </div>
      </div>
    );
  }
}

export function GenerateImage({ device, attribute, scale }) {
  const image = useSelector((state: IRootState) => {
    return getAttributeLastValueFromState(state.messages, device, attribute);
  });

  if (!image) {
    return (
      <div>
        No data found for {device}/{attribute}
      </div>
    );
  }

  try {
    const canvas = document.createElement("canvas");
    const height = image.length;
    const width = image[0].length;

    let max = Number(image[0][0]);
    for (let x = 0; x < width; x++) {
      for (let y = 0; y < height; y++) {
        const pixel = Number(image[y][x]);
        if (pixel > max) {
          max = pixel;
        }
      }
    }

    const context = canvas.getContext("2d");
    if (context == null) {
      return <div>Context not found</div>;
    }

    const imgData = context.createImageData(width, height);

    for (let x = 0; x < width; x++) {
      for (let y = 0; y < height; y++) {
        const value = image[y][x];
        const index = y * width * 4 + x * 4;
        const normal = 255 * (Number(value) / (max === 0 ? 1 : max));
        imgData.data[index + 0] = normal;
        imgData.data[index + 1] = normal;
        imgData.data[index + 2] = normal;
        imgData.data[index + 3] = 255;
      }
    }

    context.putImageData(imgData, 0, 0);
    let img = new Image();
    img.src = canvas.toDataURL("image/png");

    return (
      <img
        src={img.src}
        alt="Failed at fetching the device attribute"
        style={{
          width: scale,
          height: scale,
        }}
      />
    );
  } catch (e) {
    return <div>Error found</div>;
  }
}

const definition: WidgetDefinition<Inputs> = {
  type: "Image_Display",
  name: "Image Display",
  historyLimit: 1,
  defaultWidth: 30,
  defaultHeight: 20,
  inputs: {
    attribute: {
      label: "",
      type: "attribute",
      dataFormat: "image",
      required: true,
    },
    showAttribute: {
      type: "select",
      label: "Attribute display:",
      default: "Label",
      options: [
        {
          name: "Label",
          value: "Label",
        },
        {
          name: "Name",
          value: "Name",
        },
        {
          name: "None",
          value: "None",
        },
      ],
    },
    showDevice: {
      type: "boolean",
      label: "Device Name",
      default: false,
    },
    showTangoDB: {
      type: "boolean",
      label: "Show Tango database name",
      default: false,
    },
    scale: {
      type: "string",
      default: "1",
    },
    textColor: {
      label: "Text Color",
      type: "color",
      default: "#000000",
    },
    backgroundColor: {
      label: "Background Color",
      type: "color",
      default: "#ffffff",
    },
    size: {
      label: "Text size (in units)",
      type: "number",
      default: 1,
      nonNegative: true,
    },
    font: {
      type: "select",
      default: "Helvetica",
      label: "Font type",
      options: [
        {
          name: "Default (Helvetica)",
          value: "Helvetica",
        },
        {
          name: "Monospaced (Courier new)",
          value: "Courier new",
        },
      ],
    },
    widgetCss: {
      type: "style",
      default: "",
      label: "Custom Css",
    },
  },
};

const ImageDisplayExport = { component: ImageDisplay, definition };
export default ImageDisplayExport;
