import React from "react";
import { AttributeInput } from "../../types";

import { configure, shallow } from "enzyme";
import Adapter from "@cfaester/enzyme-adapter-react-18";
import ImageDisplay from "./ImageDisplay";
import { useSelector as useSelectorMock } from "react-redux";
import imgSample from "./image-sample.png";

jest.mock("react-redux", () => ({
  useSelector: jest.fn(),
}));
const useSelector = useSelectorMock as jest.Mock;
configure({ adapter: new Adapter() });

interface Input {
  showAttribute: string;
  attribute: AttributeInput;
  scale: string;
  showDevice: boolean;
  showTangoDB: boolean;
  textColor: string;
  backgroundColor: string;
  size: number;
  font: string;
  widgetCss: any;
}

let myAttributeInput: AttributeInput;
let myInput: Input;
var writeArray: any = [];
var date = new Date();
var timestamp = date.getTime();

describe("ImageDisplayTests", () => {
  beforeEach(() => {
    useSelector.mockImplementation((selectorFn) =>
      selectorFn({
        messages: {
          "sys/tg_test/1": {
            attributes: {
              long_scalar: {
                quality: ["ATTR_VALID"],
                timestamp: [1675353614.375299],
                values: [
                  [
                    [1, 2, 3],
                    [1, 2, 3],
                    [1, 2, 3],
                  ],
                ],
              },
            },
          },
        },
        ui: {
          mode: "run",
        },
      })
    );
  });
  it("renders all false without crashing", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "double_image_ro",
      label: "double_image_ro",
      history: [],
      dataType: "image",
      dataFormat: "",
      isNumeric: false,
      unit: "",
      enumlabels: [],
      write: writeArray,
      value: [
        [0, 0],
        [0, 0],
      ],
      writeValue: "",
      timestamp: timestamp,
      quality: "VALID",
    };

    myInput = {
      showAttribute: "",
      attribute: myAttributeInput,
      scale: "",
      showDevice: false,
      showTangoDB: false,
      textColor: "",
      backgroundColor: "",
      size: 1,
      font: "",
      widgetCss: [],
    };
    window.HTMLCanvasElement.prototype.getContext = () => {
      return null;
    };
    const element = React.createElement(ImageDisplay.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
      id: 42,
    });
    expect(shallow(element).html()).toContain("ImageDisplay");
    expect(shallow(element).html()).toContain(
      "No data found for sys/tg_test/1/double_image_ro"
    );
  });

  it("renders all true without crashing", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "",
      label: "",
      history: [],
      dataType: "",
      dataFormat: "",
      isNumeric: true,
      unit: "",
      enumlabels: [],
      write: writeArray,
      value: undefined,
      writeValue: "",
      timestamp: timestamp,
      quality: "VALID",
    };

    myInput = {
      showAttribute: "Name",
      attribute: myAttributeInput,
      scale: "1",
      showDevice: true,
      showTangoDB: false,
      textColor: "black",
      backgroundColor: "white",
      size: 1,
      font: "Helvetica",
      widgetCss: [],
    };
    window.HTMLCanvasElement.prototype.getContext = (cxt) => {
      return cxt;
    };

    const element = React.createElement(ImageDisplay.component, {
      mode: "edit",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
      id: 42,
    });

    expect(shallow(element).html()).toContain(
      "Failed at fetching the device attribute"
    );
    expect(shallow(element).html()).toContain(imgSample);
  });

  it("renders in edit mode before device and attribute are set", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "long_scalar",
      label: "long_scalar",
      history: [],
      dataType: "",
      dataFormat: "",
      isNumeric: true,
      unit: "",
      enumlabels: [],
      write: writeArray,
      value: [],
      writeValue: "",
      timestamp: timestamp,
      quality: "VALID",
    };

    myInput = {
      showAttribute: "Name",
      attribute: myAttributeInput,
      scale: "1",
      showDevice: true,
      showTangoDB: false,
      textColor: "black",
      backgroundColor: "white",
      size: 1,
      font: "Helvetica",
      widgetCss: [],
    };
    window.HTMLCanvasElement.prototype.getContext = (cxt) => {
      return cxt;
    };
    const element = React.createElement(ImageDisplay.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
      id: 42,
    });

    expect(shallow(element).html()).toContain("Error found");
  });

  it("renders in edit mode for empty device", () => {
    myAttributeInput = {
      device: "",
      attribute: "long_scalar",
      label: "long_scalar",
      history: [],
      dataType: "",
      dataFormat: "",
      isNumeric: true,
      unit: "",
      enumlabels: [],
      write: writeArray,
      value: [],
      writeValue: "",
      timestamp: timestamp,
      quality: "VALID",
    };

    myInput = {
      showAttribute: "Name",
      attribute: myAttributeInput,
      scale: "1",
      showDevice: true,
      showTangoDB: false,
      textColor: "black",
      backgroundColor: "white",
      size: 1,
      font: "Helvetica",
      widgetCss: [],
    };
    window.HTMLCanvasElement.prototype.getContext = (cxt) => {
      return cxt;
    };
    const element = React.createElement(ImageDisplay.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
      id: 42,
    });

    expect(shallow(element).html()).toContain("device");
  });

  it("does not display the device name if showDevice is not set", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "double_image_ro",
      label: "double_image_ro",
      history: [],
      dataType: "",
      dataFormat: "",
      isNumeric: true,
      unit: "",
      enumlabels: [],
      write: writeArray,
      value: [
        [0, 0],
        [0, 0],
      ],
      writeValue: "",
      timestamp: timestamp,
      quality: "VALID",
    };

    myInput = {
      showAttribute: "Name",
      attribute: myAttributeInput,
      scale: "1",
      showDevice: false,
      showTangoDB: false,
      textColor: "black",
      backgroundColor: "white",
      size: 1,
      font: "Helvetica",
      widgetCss: [],
    };
    window.HTMLCanvasElement.prototype.getContext = (cxt) => {
      return cxt;
    };

    const element = React.createElement(ImageDisplay.component, {
      mode: "edit",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
      id: 42,
    });
    expect(shallow(element).html()).not.toContain("sys/tg_test/1");
  });

  it("display the device attribute if showAttribute is set", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "double_image_ro",
      label: "double_image_ro",
      history: [],
      dataType: "",
      dataFormat: "",
      isNumeric: true,
      unit: "",
      enumlabels: [],
      write: writeArray,
      value: [
        [0, 0],
        [0, 0],
      ],
      writeValue: "",
      timestamp: timestamp,
      quality: "VALID",
    };

    myInput = {
      showAttribute: "Label",
      attribute: myAttributeInput,
      scale: "1",
      showDevice: false,
      showTangoDB: false,
      textColor: "black",
      backgroundColor: "white",
      size: 1,
      font: "Helvetica",
      widgetCss: [],
    };

    window.HTMLCanvasElement.prototype.getContext = (cxt) => {
      return cxt;
    };
    const element = React.createElement(ImageDisplay.component, {
      mode: "edit",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
      id: 42,
    });
    expect(shallow(element).html()).toContain("double_image_ro");
  });

  it("Empty context object", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "long_scalar",
      label: "long_scalar1",
      history: [],
      dataType: "",
      dataFormat: "",
      isNumeric: true,
      unit: "",
      enumlabels: [],
      write: writeArray,
      value: [
        [0, 0],
        [0, 0],
      ],
      writeValue: "",
      timestamp: timestamp,
      quality: "VALID",
    };

    myInput = {
      showAttribute: "Label",
      attribute: myAttributeInput,
      scale: "1",
      showDevice: false,
      showTangoDB: false,
      textColor: "black",
      backgroundColor: "white",
      size: 1,
      font: "Helvetica",
      widgetCss: [],
    };

    window.HTMLCanvasElement.prototype.getContext = (cxt) => {
      return null;
    };
    const element = React.createElement(ImageDisplay.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
      id: 42,
    });
    expect(shallow(element).html()).toContain("Context not found");
  });
});

describe("test Generate Image", () => {
  const mockCreateImageData = jest.fn();
  const mockPutImageData = jest.fn();

  beforeEach(() => {
    useSelector.mockImplementation((selectorFn) =>
      selectorFn({
        messages: {
          "sys/tg_test/1": {
            attributes: {
              long_scalar: {
                quality: ["ATTR_VALID"],
                timestamp: [1675353614.375299],
                values: [
                  [
                    [1, 2, 3],
                    [1, 2, 3],
                    [1, 2, 3],
                  ],
                ],
              },
            },
          },
        },
        ui: {
          mode: "run",
        },
      })
    );

    const contx = {
      createImageData: mockCreateImageData,
      putImageData: mockPutImageData,
    };
    const contxx = jest.fn();
    (contxx as jest.Mock).mockReturnValue(contx);

    const canv = {
      getContext: contxx,
      toDataURL: jest.fn(),
    };
    document.createElement = jest.fn().mockReturnValueOnce(canv);
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  it("test Generate Image good path", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "long_scalar",
      label: "long_scalar",
      history: [],
      dataType: "",
      dataFormat: "",
      isNumeric: true,
      unit: "",
      enumlabels: [],
      write: writeArray,
      value: [],
      writeValue: "",
      timestamp: timestamp,
      quality: "VALID",
    };

    myInput = {
      showAttribute: "Name",
      attribute: myAttributeInput,
      scale: "1",
      showDevice: true,
      showTangoDB: false,
      textColor: "black",
      backgroundColor: "white",
      size: 1,
      font: "Helvetica",
      widgetCss: [],
    };
    window.HTMLCanvasElement.prototype.getContext = (cxt) => {
      return cxt;
    };

    const element = React.createElement(ImageDisplay.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
      id: 42,
    });

    shallow(element).html();
    expect(mockCreateImageData).toHaveBeenCalledTimes(1);
  });
});
