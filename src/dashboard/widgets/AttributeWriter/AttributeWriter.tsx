import React, { Component, FormEvent, CSSProperties } from "react";

import { WidgetProps } from "../types";
import {
  WidgetDefinition,
  StringInputDefinition,
  AttributeInputDefinition,
  BooleanInputDefinition,
  ColorInputDefinition,
  NumberInputDefinition,
  SelectInputDefinition,
  AttributeInput,
  StyleInputDefinition,
} from "../../types";

import { parseCss } from "../../components/Inspector/StyleSelector";
import AttributeValues from "./AttributeValues";
import { showHideTangoDBName } from "../../DBHelper";

type Inputs = {
  title: StringInputDefinition;
  attribute: AttributeInputDefinition;
  showDevice: BooleanInputDefinition;
  showTangoDB: BooleanInputDefinition;
  showAttribute: SelectInputDefinition;
  alignValueRight: BooleanInputDefinition;
  textColor: ColorInputDefinition;
  backgroundColor: ColorInputDefinition;
  size: NumberInputDefinition;
  font: SelectInputDefinition;
  widgetCss: StyleInputDefinition;
};

type Props = WidgetProps<Inputs>;

interface State {
  input: string;
  pending: boolean;
  validation: boolean;
}

enum DATA_TYPES {
  BOOLEAN = "boolean",
  NUMERIC = "numeric",
  STRING = "string",
  DEV_ENUM = "devenum",
  DEV_DOUBLE = "DevDouble",
  OTHER = "other",
}

class AttributeWriter extends Component<Props, State> {
  public constructor(props: Props) {
    super(props);
    const { dataType } = this.props.inputs.attribute;
    const defaultValues = {
      DevEnum: "0",
      DevBoolean: "t",
    };
    this.state = {
      input: dataType in defaultValues ? defaultValues[dataType] : "",
      pending: false,
      validation: false,
    };

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  public render() {
    const { mode, inputs } = this.props;
    const {
      title,
      attribute,
      showDevice,
      showTangoDB,
      showAttribute,
      alignValueRight,
      backgroundColor,
      textColor,
      size,
      font,
      widgetCss,
    } = inputs;
    let {
      device,
      value,
      dataType,
      minAlarm,
      maxAlarm,
      minValue,
      maxValue,
    } = attribute;

    const WidgetDivCss = parseCss(widgetCss).data;
    const unit = mode === "run" ? attribute.unit : "unit";

    let display = this.getDisplay(attribute, showAttribute);
    const deviceLabel = showHideTangoDBName(
      showDevice,
      showTangoDB,
      device,
      "Device"
    );

    const displayWidget = [...[deviceLabel], display].join("/");

    let dataTypeCategory = this.attributeDataType();
    if (
      mode === "run" &&
      dataTypeCategory !== DATA_TYPES.NUMERIC &&
      dataTypeCategory !== DATA_TYPES.STRING &&
      dataTypeCategory !== DATA_TYPES.BOOLEAN &&
      dataTypeCategory !== DATA_TYPES.DEV_ENUM
    ) {
      return (
        <div style={{ backgroundColor: "red" }}>{dataType} not implemented</div>
      );
    }

    let isInvalid = true;
    if (this.state.input !== "") {
      isInvalid =
        ((dataTypeCategory === DATA_TYPES.NUMERIC ||
          dataTypeCategory === DATA_TYPES.DEV_ENUM) &&
          isNaN(Number(this.state.input)) &&
          this.state.input !== "") ||
        (dataTypeCategory === DATA_TYPES.BOOLEAN &&
          this.state.input !== "" &&
          ["true", "false", ""].indexOf(this.state.input.toLowerCase()) === -1);
    }

    if (
      !isInvalid &&
      minValue !== undefined &&
      minValue !== null &&
      maxValue !== undefined &&
      maxValue !== null
    )
      isInvalid =
        Number(this.state.input) < minValue ||
        Number(this.state.input) > maxValue;

    if (mode === "edit") {
      dataType = "DevString";
      value = "...";
      dataTypeCategory = DATA_TYPES.STRING;
      isInvalid = false;
    }

    let style: CSSProperties = {
      alignItems: "center",
      backgroundColor,
      color: textColor,
      fontSize: size + "em",
      height: "100%",
      ...WidgetDivCss,
    };
    if (font) {
      style["fontFamily"] = font;
    }

    const submitButton =
      dataTypeCategory === DATA_TYPES.BOOLEAN ||
      dataTypeCategory === DATA_TYPES.DEV_ENUM ? (
        <button
          style={{ padding: "revert" }}
          className="btn btn-primary btn-dashboard"
          type={"submit"}
        >
          Write
        </button>
      ) : null;
    let placeholder =
      dataTypeCategory === DATA_TYPES.NUMERIC &&
      dataType === DATA_TYPES.DEV_DOUBLE &&
      value
        ? value.toFixed(2)
        : value;

    return (
      <div id="AttributeWriter" style={style}>
        <form
          style={{ display: "flex" }}
          className="justify-content-left"
          onSubmit={this.handleSubmit}
        >
          <div>
            <span style={{ flexGrow: 0 }}>{title}</span>
            {displayWidget && (
              <span style={{ flexGrow: 0 }}>{displayWidget}:</span>
            )}
          </div>
          <AttributeValues
            mode={mode}
            attributeName={attribute.attribute}
            deviceName={attribute.device}
            alarms={[minAlarm, maxAlarm]}
            bounds={[minValue, maxValue]}
            type={dataTypeCategory}
            value={this.state.input}
            isValid={!isInvalid}
            placeholder={placeholder}
            validating={this.state.validation}
            onFocus={() => this.setState({ validation: true })}
            onBlur={() => this.setState({ validation: false })}
            onChange={(e) => this.setState({ input: e.target.value })}
            alignValueRight={alignValueRight}
            enumLabels={attribute.enumlabels}
          />
          {submitButton}
          <div>
            {unit && <span style={{ marginLeft: "0.5em" }}>{unit}</span>}
          </div>
        </form>
      </div>
    );
  }

  private attributeDataType() {
    const { attribute } = this.props.inputs;
    const { dataType, isNumeric } = attribute;

    if (isNumeric) {
      return DATA_TYPES.NUMERIC;
    }
    if (dataType === "DevBoolean") {
      return DATA_TYPES.BOOLEAN;
    }
    if (dataType === "DevString") {
      return DATA_TYPES.STRING;
    }
    if (dataType === "DevEnum") {
      return DATA_TYPES.DEV_ENUM;
    }
    return DATA_TYPES.OTHER;
  }

  private getDisplay(attribute: AttributeInput, showAttribute: string): string {
    let display = "";
    if (showAttribute === "Label") {
      if (attribute.label !== "") display = attribute.label;
      else display = "Attribute";
    } else if (showAttribute === "Name") {
      if (attribute.attribute !== "") display = attribute.attribute;
      else display = "attributeName";
    }
    return display;
  }

  private async handleSubmit(event: FormEvent<HTMLFormElement>) {
    event.preventDefault();

    const { attribute } = this.props.inputs;
    const { isNumeric, minValue, maxValue } = attribute;
    const isBoolean = attribute.dataType === "DevBoolean";
    const isDevEnum = attribute.dataType === "DevEnum";
    const { input } = this.state;
    let value: any = input;

    if (this.state.pending) {
      alert("The request is still pending. Please wait..");
      return;
    }

    if (value === "") {
      alert("Input cannot be empty.");
      return; //we don't want to interpret an emtpy string as a zero or false
    }

    if (isNumeric || isDevEnum) {
      value = Number(input);
    }
    if (isBoolean) {
      value = input.toLowerCase() === "f" ? false : true;
    }
    if (typeof value === "number" && isNaN(value)) {
      alert("Invalid number entered.");
      return;
    }
    if (maxValue !== undefined && maxValue !== null) {
      if (typeof value === "number" && value > maxValue) {
        alert(`Value must be less than or equal to ${maxValue}`);
        return;
      }
    }
    if (minValue !== undefined && minValue !== null) {
      if (typeof value === "number" && value < minValue) {
        alert(`Value must be greater than or equal to ${minValue}`);
        return;
      }
    }

    const clearInput = !isBoolean && !isDevEnum;

    this.setState({ input: clearInput ? "" : input, pending: true });
    await this.props.inputs.attribute.write(value);
    this.setState({ pending: false });
  }
}

const definition: WidgetDefinition<Inputs> = {
  type: "ATTRIBUTE_WRITER",
  name: " Attribute Writer",
  defaultHeight: 2,
  defaultWidth: 20,
  inputs: {
    title: {
      type: "string",
      label: "Title",
      default: "",
      placeholder: "Title of widget",
    },
    attribute: {
      type: "attribute",
      label: "",
      dataFormat: "scalar",
    },
    showDevice: {
      type: "boolean",
      label: "Show Device Name",
      default: true,
    },
    showTangoDB: {
      type: "boolean",
      label: "Show Tango database name",
      default: false,
    },
    showAttribute: {
      type: "select",
      label: "Attribute display:",
      default: "Label",
      options: [
        {
          name: "Label",
          value: "Label",
        },
        {
          name: "Name",
          value: "Name",
        },
        {
          name: "None",
          value: "None",
        },
      ],
    },
    alignValueRight: {
      type: "boolean",
      label: "Align value on right",
      default: true,
    },
    textColor: {
      label: "Text Color",
      type: "color",
      default: "#000000",
    },
    backgroundColor: {
      label: "Background Color",
      type: "color",
      default: "#ffffff",
    },
    size: {
      label: "Text size (in units)",
      type: "number",
      default: 1,
      nonNegative: true,
    },
    font: {
      type: "select",
      default: "Helvetica",
      label: "Font type",
      options: [
        {
          name: "Default (Helvetica)",
          value: "Helvetica",
        },
        {
          name: "Monospaced (Courier new)",
          value: "Courier new",
        },
      ],
    },
    widgetCss: {
      type: "style",
      default: "",
      label: "Custom CSS",
    },
  },
};

const AttributeWriterExport = { definition, component: AttributeWriter };
export default AttributeWriterExport;
