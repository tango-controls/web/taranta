import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import {
  useSelector,// as userSelectorMock,
  useDispatch// as useDispatchMock 
} from 'react-redux';
import AttributeWriterButton from './AttributeWriterButton';
import { setDeviceAttribute } from '../../../shared/state/actions/tango';

const device = 'sys/tg_test/1';
const attribute = 'ampli';
const submitButtonCss = 'SubmitButtonCSS';
const variant = 'primary';
const type = 'submit';
const className = 'buttonClassName';
const submitButtonTitle = 'Submit';


// Mocking react-redux useSelector and useDispatch
jest.mock('react-redux', () => ({
  useSelector: jest.fn(),
  useDispatch: jest.fn(),
}));
// const useSelector = userSelectorMock as jest.Mock;
// const useDispatch = useDispatchMock as jest.Mock;
// Mocking the getTangoDB function
jest.mock('../../../dashboard/dashboardRepo', () => ({
  getTangoDB: jest.fn(),
}));

describe('AttributeWriterButton', () => {
  // Mocking the useDispatch hook
  const dispatch = jest.fn();
  // Mocking useSelector to return a selected value
  beforeEach(() => {
      (useDispatch as jest.Mock).mockReturnValue(dispatch);
      (useSelector as jest.Mock).mockReturnValue( "testDB" );
    });

  afterEach(() => {
      jest.resetAllMocks();
    });

  it('should dispatch setDeviceAttribute action on button click and convert Number', () => {
    // Mocking getTangoDB to return the Tango DB name
    const mockTangoDB = "testDB";
    const isNumeric = true;
    const isBoolean = false;
    const isEnum = false;
    const selected = { title: 'one', value: '1' } 
    require('../../../dashboard/dashboardRepo').getTangoDB.mockReturnValue(mockTangoDB);

    const { getByText } = render(
      <AttributeWriterButton
        device={device}
        attribute={attribute}
        selected={selected}
        submitButtonCss={submitButtonCss}
        variant={variant}
        type={type}
        className={className}
        submitButtonTitle={submitButtonTitle}
        isNumeric={isNumeric}
        isBoolean={isBoolean}
        isEnum={isEnum}
      />
    );

    const submitButton = getByText(submitButtonTitle);
    fireEvent.click(submitButton);

    expect(dispatch).toHaveBeenCalledWith(
      setDeviceAttribute(mockTangoDB, device, attribute, Number(selected.value))
    );
  });

  it('button is disabled if selected is null', () => {
    // Mocking getTangoDB to return the Tango DB data
    const mockTangoDB = "testDB";
    const isNumeric = true;
    const isBoolean = false;
    const isEnum = false;
    const selected = null;
    require('../../../dashboard/dashboardRepo').getTangoDB.mockReturnValue(mockTangoDB);

    const renderedComponent = render(
      <AttributeWriterButton
        device={device}
        attribute={attribute}
        selected={selected}
        submitButtonCss={submitButtonCss}
        variant={variant}
        type={type}
        className={className}
        submitButtonTitle={submitButtonTitle}
        isNumeric={isNumeric}
        isBoolean={isBoolean}
        isEnum={isEnum}
      />
    );

    expect(renderedComponent.container.innerHTML).toContain("disable")
  });

  it('converts boolean if value is True', () => {
    // Mocking getTangoDB to return the Tango DB data
    const mockTangoDB = "testDB";
    const isNumeric = false;
    const isBoolean = true;
    const isEnum = false;
    const selected = { title: 'True', value: 'true' } 
    require('../../../dashboard/dashboardRepo').getTangoDB.mockReturnValue(mockTangoDB);



    const { getByText } = render(
      <AttributeWriterButton
        device={device}
        attribute={attribute}
        selected={selected}
        submitButtonCss={submitButtonCss}
        variant={variant}
        type={type}
        className={className}
        submitButtonTitle={submitButtonTitle}
        isNumeric={isNumeric}
        isBoolean={isBoolean}
        isEnum={isEnum}
      />
    );

    const submitButton = getByText(submitButtonTitle);
    fireEvent.click(submitButton);

    expect(dispatch).toHaveBeenCalledWith(
      setDeviceAttribute(mockTangoDB, device, attribute, true)
    );
    
  });

  it('converts boolean if value is False', () => {
    // Mocking getTangoDB to return the Tango DB data
    const mockTangoDB = "testDB";
    const isNumeric = false;
    const isBoolean = true;
    const isEnum = false;
    const selected = { title: 'False', value: 'false' } 
    require('../../../dashboard/dashboardRepo').getTangoDB.mockReturnValue(mockTangoDB);



    const { getByText } = render(
      <AttributeWriterButton
        device={device}
        attribute={attribute}
        selected={selected}
        submitButtonCss={submitButtonCss}
        variant={variant}
        type={type}
        className={className}
        submitButtonTitle={submitButtonTitle}
        isNumeric={isNumeric}
        isBoolean={isBoolean}
        isEnum={isEnum}
      />
    );

    const submitButton = getByText("Submit");
    fireEvent.click(submitButton);

    expect(dispatch).toHaveBeenCalledWith(
      setDeviceAttribute(mockTangoDB, device, attribute, false)
    );
    
  });

  it('converts boolean if value is wrong', () => {
    // Mocking getTangoDB to return the Tango DB data
    const mockTangoDB = "testDB";
    const isNumeric = false;
    const isBoolean = true;
    const isEnum = false;
    const selected = { title: 'False', value: '5' } 
    require('../../../dashboard/dashboardRepo').getTangoDB.mockReturnValue(mockTangoDB);



    const { getByText } = render(
      <AttributeWriterButton
        device={device}
        attribute={attribute}
        selected={selected}
        submitButtonCss={submitButtonCss}
        variant={variant}
        type={type}
        className={className}
        submitButtonTitle={submitButtonTitle}
        isNumeric={isNumeric}
        isBoolean={isBoolean}
        isEnum={isEnum}
      />
    );

    const submitButton = getByText("Submit");
    fireEvent.click(submitButton);

    expect(dispatch).toHaveBeenCalledWith(
      setDeviceAttribute(mockTangoDB, device, attribute, null)
    );
    
  });

  it('converts enum', () => {
    // Mocking getTangoDB to return the Tango DB data
    const mockTangoDB = "testDB";
    const isNumeric = false;
    const isBoolean = false;
    const isEnum = true;
    const selected = { title: 'RUNNING', value: '5' } 
    require('../../../dashboard/dashboardRepo').getTangoDB.mockReturnValue(mockTangoDB);



    let { getByText } = render(
      <AttributeWriterButton
        device={device}
        attribute={attribute}
        selected={selected}
        submitButtonCss={submitButtonCss}
        variant={variant}
        type={type}
        className={className}
        submitButtonTitle={submitButtonTitle}
        isNumeric={isNumeric}
        isBoolean={isBoolean}
        isEnum={isEnum}
      />
    );

    let submitButton = getByText("Submit");
    fireEvent.click(submitButton);

    expect(dispatch).toHaveBeenCalledWith(
      setDeviceAttribute(mockTangoDB, device, attribute, 5)
    );
  });

  it('converts enum and if values is wrong, dispatch is not called', () => {
    // Mocking getTangoDB to return the Tango DB data
    const mockTangoDB = "testDB";
    const isNumeric = false;
    const isBoolean = false;
    const isEnum = true;
    const selected = { title: 'RUNNING', value: 'NaN' } 
    require('../../../dashboard/dashboardRepo').getTangoDB.mockReturnValue(mockTangoDB);



    let { getByText } = render(
      <AttributeWriterButton
        device={device}
        attribute={attribute}
        selected={selected}
        submitButtonCss={submitButtonCss}
        variant={variant}
        type={type}
        className={className}
        submitButtonTitle={submitButtonTitle}
        isNumeric={isNumeric}
        isBoolean={isBoolean}
        isEnum={isEnum}
      />
    );

    let submitButton = getByText("Submit");
    fireEvent.click(submitButton);

    expect(dispatch).not.toHaveBeenCalled();
  });

  it('if selected is null, dispatch is not called', () => {
    // Mocking getTangoDB to return the Tango DB data
    const mockTangoDB = "testDB";
    const isNumeric = true;
    const isBoolean = false;
    const isEnum = false;
    const selected = null;
    require('../../../dashboard/dashboardRepo').getTangoDB.mockReturnValue(mockTangoDB);



    let { getByText } = render(
      <AttributeWriterButton
        device={device}
        attribute={attribute}
        selected={selected}
        submitButtonCss={submitButtonCss}
        variant={variant}
        type={type}
        className={className}
        submitButtonTitle={submitButtonTitle}
        isNumeric={isNumeric}
        isBoolean={isBoolean}
        isEnum={isEnum}
      />
    );

    let submitButton = getByText("Submit");
    fireEvent.click(submitButton);

    expect(dispatch).not.toHaveBeenCalled();
  });

  it('with a different button name', () => {
    // Mocking getTangoDB to return the Tango DB data
    const submitButtonTitle:any = null;
    const mockTangoDB = "testDB";
    const isNumeric = true;
    const isBoolean = false;
    const isEnum = false;
    const selected = { title: 'one', value: '1' } ;
    require('../../../dashboard/dashboardRepo').getTangoDB.mockReturnValue(mockTangoDB);

    let { getByText } = render(
      <AttributeWriterButton
        device={device}
        attribute={attribute}
        selected={selected}
        submitButtonCss={submitButtonCss}
        variant={variant}
        type={type}
        className={className}
        submitButtonTitle={submitButtonTitle}
        isNumeric={isNumeric}
        isBoolean={isBoolean}
        isEnum={isEnum}
      />
    );

    let submitButton = getByText("Submit");
    fireEvent.click(submitButton);

    expect(dispatch).toHaveBeenCalled();
  });

});