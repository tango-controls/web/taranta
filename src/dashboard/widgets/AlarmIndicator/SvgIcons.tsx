import React from "react";
import { SvgLoader } from "react-svgmt";
import { ALARM_STATUS_RTNUN, ALARM_STATUS_SHLVD, ALARM_STATUS_UNACK, FAULT_ALARM, LOG_ALARM, WARNING_ALARM } from "./AlarmConst";

export const getSvgIcon = ({ priority, enumValue, mode }) => {

   const CssClass = enumValue === ALARM_STATUS_UNACK ? 'blinking' :
                  enumValue === ALARM_STATUS_RTNUN ? 'opacity-50' : '';

   if ("edit" === mode) {
      enumValue = ALARM_STATUS_RTNUN
      priority = LOG_ALARM
   }
   return <>
      {(priority === LOG_ALARM) &&
         <div className={"svg-container "+CssClass}>
            <SvgLoader svgXML={lowPriorityAlarms?.[enumValue]}></SvgLoader>
         </div>
      }
      {(priority === WARNING_ALARM) &&
         <div className={"svg-container "+CssClass}>
            <SvgLoader svgXML={mediumPriorityAlarms?.[enumValue]}></SvgLoader>
         </div>
      }
      {
         ((priority === FAULT_ALARM)) &&
         <div className={"svg-container "+CssClass}>
            <SvgLoader svgXML={highPriorityAlarms?.[enumValue]}></SvgLoader>
         </div>
      }
   </>
}
const MemoizedGetSvgIcon = React.memo(getSvgIcon)
export default MemoizedGetSvgIcon;

const ACK_HIGH = `
<svg
   width="81"
   height="81"
   viewBox="0 0 23.279016 23.279018"
   version="1.1"
   id="svg1"
   sodipodi:docname="drawing.svg"
   inkscape:version="1.3.2 (091e20e, 2023-11-25, custom)"
   xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"
   xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"
   xmlns="http://www.w3.org/2000/svg"
   xmlns:svg="http://www.w3.org/2000/svg">
   <sodipodi:namedview
      id="namedview1"
      pagecolor="#ffffff"
      bordercolor="#000000"
      borderopacity="0.25"
      inkscape:showpageshadow="2"
      inkscape:pageopacity="0.0"
      inkscape:pagecheckerboard="0"
      inkscape:deskcolor="#d1d1d1"
      inkscape:document-units="mm"
      inkscape:zoom="0.71446409"
      inkscape:cx="396.80091"
      inkscape:cy="449.2878"
      inkscape:window-width="1708"
      inkscape:window-height="871"
      inkscape:window-x="2391"
      inkscape:window-y="-9"
      inkscape:window-maximized="1"
      inkscape:current-layer="layer1" />
   <defs
      id="defs1" />
   <g
      inkscape:label="Layer 1"
      inkscape:groupmode="layer"
      id="layer1">
      <g
      id="g9-9"
      inkscape:label="ACK / UNACK"
      transform="matrix(3.5963615,0,0,3.5963615,-152.04873,-113.78306)"
      style="display:inline">
      <path
            id="rect1-6-0"
            style="fill:#ad1f1f;fill-opacity:1;stroke:#ad1f1f;stroke-width:0;stroke-linejoin:bevel;stroke-opacity:0.901961"
            d="m 42.597917,37.205952 2.899532,-4.662202 2.934545,4.653532 z"
            sodipodi:nodetypes="cccc"
            inkscape:label="rect1-6" />
      <text
            xml:space="preserve"
            style="font-size:2.64583px;line-height:0;font-family:Sans;-inkscape-font-specification:'Sans, Normal';word-spacing:0px;display:inline;fill:#ffffff;fill-opacity:1;stroke:none;stroke-width:3.17"
            x="44.496681"
            y="36.432377"
            id="text1-1-9"
            inkscape:label="H"><tspan
            sodipodi:role="line"
            id="tspan1-5-3"
            style="fill:#ffffff;fill-opacity:1;stroke:none;stroke-width:3.17"
            x="44.496681"
            y="36.432377">H</tspan></text>
      </g>
   </g>
</svg>`
const ACK_MED = `
<svg
   width="81"
   height="81"
   viewBox="0 0 23.279016 23.279018"
   version="1.1"
   id="svg1"
   sodipodi:docname="drawing.svg"
   inkscape:version="1.3.2 (091e20e, 2023-11-25, custom)"
   xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"
   xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"
   xmlns="http://www.w3.org/2000/svg"
   xmlns:svg="http://www.w3.org/2000/svg">
  <sodipodi:namedview
     id="namedview1"
     pagecolor="#ffffff"
     bordercolor="#000000"
     borderopacity="0.25"
     inkscape:showpageshadow="2"
     inkscape:pageopacity="0.0"
     inkscape:pagecheckerboard="0"
     inkscape:deskcolor="#d1d1d1"
     inkscape:document-units="mm"
     inkscape:zoom="0.71446409"
     inkscape:cx="396.80091"
     inkscape:cy="449.2878"
     inkscape:window-width="1708"
     inkscape:window-height="871"
     inkscape:window-x="2391"
     inkscape:window-y="-9"
     inkscape:window-maximized="1"
     inkscape:current-layer="layer1" />
  <defs
     id="defs1" />
  <g
     inkscape:label="Layer 1"
     inkscape:groupmode="layer"
     id="layer1">
    <g
       id="g15"
       inkscape:label="ACK / UNACK"
       transform="matrix(3.5963618,0,0,3.5963618,-151.69281,-115.39339)">
      <rect
         style="fill:#fdd835;fill-opacity:1;stroke:none;stroke-width:2.91841;stroke-opacity:0.901961"
         id="rect19"
         width="4.5619702"
         height="4.5619702"
         x="54.809826"
         y="-9.4180784"
         transform="rotate(45)" />
      <text
         xml:space="preserve"
         style="font-size:2.64583px;line-height:0;font-family:Sans;-inkscape-font-specification:'Sans, Normal';word-spacing:0px;display:inline;fill:#000000;fill-opacity:1;stroke:none;stroke-width:3.17"
         x="44.354397"
         y="36.375462"
         id="text14"
         inkscape:label="M"><tspan
           sodipodi:role="line"
           id="tspan14"
           style="fill:#000000;fill-opacity:1;stroke:none;stroke-width:3.17"
           x="44.354397"
           y="36.375462">M</tspan></text>
    </g>
  </g>
</svg>`
const ACK_LOW = `
<svg
   width="81"
   height="81"
   viewBox="0 0 23.279014 23.279013"
   version="1.1"
   id="svg1"
   sodipodi:docname="drawing.svg"
   inkscape:version="1.3.2 (091e20e, 2023-11-25, custom)"
   xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"
   xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"
   xmlns="http://www.w3.org/2000/svg"
   xmlns:svg="http://www.w3.org/2000/svg">
  <sodipodi:namedview
     id="namedview1"
     pagecolor="#ffffff"
     bordercolor="#000000"
     borderopacity="0.25"
     inkscape:showpageshadow="2"
     inkscape:pageopacity="0.0"
     inkscape:pagecheckerboard="0"
     inkscape:deskcolor="#d1d1d1"
     inkscape:document-units="mm"
     inkscape:zoom="0.71446409"
     inkscape:cx="396.80091"
     inkscape:cy="449.2878"
     inkscape:window-width="1708"
     inkscape:window-height="871"
     inkscape:window-x="2391"
     inkscape:window-y="-9"
     inkscape:window-maximized="1"
     inkscape:current-layer="layer1" />
  <defs
     id="defs1" />
  <g
     inkscape:label="Layer 1"
     inkscape:groupmode="layer"
     id="layer1">
    <g
       id="g23"
       inkscape:label="ACK / UNACK"
       transform="matrix(-3.5963615,0,0,-3.5963615,175.75205,135.49516)"
       style="display:inline">
      <path
         id="path22"
         style="fill:#00d0d0;fill-opacity:1;stroke:#c62828;stroke-width:0;stroke-linejoin:bevel;stroke-opacity:0.901961"
         d="m 42.715899,36.616042 2.899532,-4.662202 2.934545,4.653532 z"
         sodipodi:nodetypes="cccc"
         inkscape:label="rect1-6" />
      <text
         xml:space="preserve"
         style="font-size:2.64583px;line-height:0;font-family:Sans;-inkscape-font-specification:'Sans, Normal';word-spacing:0px;display:inline;fill:#1a1a1a;fill-opacity:1;stroke:none;stroke-width:3.17"
         x="-46.503178"
         y="-33.946686"
         id="text22"
         inkscape:label="L"
         transform="scale(-1)"><tspan
           sodipodi:role="line"
           id="tspan22"
           style="fill:#1a1a1a;fill-opacity:1;stroke:none;stroke-width:3.17"
           x="-46.503178"
           y="-33.946686">L</tspan></text>
    </g>
  </g>
</svg>`

const OOSRV_HIGH = `
<svg
   width="81"
   height="81"
   viewBox="0 0 23.279014 23.279013"
   version="1.1"
   id="svg1"
   sodipodi:docname="drawing.svg"
   inkscape:version="1.3.2 (091e20e, 2023-11-25, custom)"
   xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"
   xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"
   xmlns="http://www.w3.org/2000/svg"
   xmlns:svg="http://www.w3.org/2000/svg">
  <sodipodi:namedview
     id="namedview1"
     pagecolor="#ffffff"
     bordercolor="#000000"
     borderopacity="0.25"
     inkscape:showpageshadow="2"
     inkscape:pageopacity="0.0"
     inkscape:pagecheckerboard="0"
     inkscape:deskcolor="#d1d1d1"
     inkscape:document-units="mm"
     inkscape:zoom="0.71446409"
     inkscape:cx="396.80091"
     inkscape:cy="449.2878"
     inkscape:window-width="1708"
     inkscape:window-height="871"
     inkscape:window-x="2391"
     inkscape:window-y="-9"
     inkscape:window-maximized="1"
     inkscape:current-layer="layer1" />
  <defs
     id="defs1" />
  <g
     inkscape:label="Layer 1"
     inkscape:groupmode="layer"
     id="layer1">
    <g
       id="g6"
       transform="matrix(2.4893904,0,0,2.4893904,-133.80644,-29.667794)"
       style="display:inline;opacity:0.8"
       inkscape:label="Out of Service (OOSERV)">
      <g
         id="g9"
         inkscape:label="ACK / UNACK"
         transform="matrix(1.4446754,0,0,1.4446754,-7.3280069,-34.176006)">
        <path
           id="rect1-6"
           style="fill:#b3b3b3;fill-opacity:1;stroke:#c62828;stroke-width:0;stroke-linejoin:bevel;stroke-opacity:0.901961"
           d="m 42.597917,37.205952 2.899532,-4.662202 2.934545,4.653532 z"
           sodipodi:nodetypes="cccc"
           inkscape:label="rect1-6" />
        <text
           xml:space="preserve"
           style="font-size:2.64583px;line-height:0;font-family:Sans;-inkscape-font-specification:'Sans, Normal';word-spacing:0px;display:inline;fill:#1a1a1a;fill-opacity:1;stroke:none;stroke-width:3.17"
           x="44.496681"
           y="36.432377"
           id="text1-1"
           inkscape:label="H"><tspan
             sodipodi:role="line"
             id="tspan1-5"
             style="fill:#1a1a1a;fill-opacity:1;stroke:none;stroke-width:3.17"
             x="44.496681"
             y="36.432377">H</tspan></text>
      </g>
      <g
         id="g10"
         inkscape:label="Ban"
         transform="translate(0.72313976,-17.837447)"
         style="stroke-width:0.346344;stroke-dasharray:none">
        <circle
           cx="57.767361"
           cy="34.484028"
           r="3.5277777"
           id="circle1"
           style="fill:none;stroke:#000000;stroke-width:0.346344;stroke-dasharray:none;stroke-opacity:1" />
        <path
           d="m 55.262639,31.979305 5.009444,5.009444"
           id="path1"
           style="fill:none;fill-opacity:0.0192307;stroke:#000000;stroke-width:0.346344;stroke-dasharray:none;stroke-opacity:1" />
      </g>
    </g>
  </g>
</svg>
`
const OOSRV_MED = `
<svg
   width="81"
   height="81"
   viewBox="0 0 23.279014 23.279013"
   version="1.1"
   id="svg1"
   sodipodi:docname="drawing.svg"
   inkscape:version="1.3.2 (091e20e, 2023-11-25, custom)"
   xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"
   xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"
   xmlns="http://www.w3.org/2000/svg"
   xmlns:svg="http://www.w3.org/2000/svg">
  <sodipodi:namedview
     id="namedview1"
     pagecolor="#ffffff"
     bordercolor="#000000"
     borderopacity="0.25"
     inkscape:showpageshadow="2"
     inkscape:pageopacity="0.0"
     inkscape:pagecheckerboard="0"
     inkscape:deskcolor="#d1d1d1"
     inkscape:document-units="mm"
     inkscape:zoom="0.71446409"
     inkscape:cx="396.80091"
     inkscape:cy="449.2878"
     inkscape:window-width="1708"
     inkscape:window-height="871"
     inkscape:window-x="2391"
     inkscape:window-y="-9"
     inkscape:window-maximized="1"
     inkscape:current-layer="layer1" />
  <defs
     id="defs1" />
  <g
     inkscape:label="Layer 1"
     inkscape:groupmode="layer"
     id="layer1">
    <g
       id="g14"
       transform="matrix(2.4893904,0,0,2.4893904,-133.63354,-30.36645)"
       style="display:inline;opacity:0.8"
       inkscape:label="Out of Service (OOSERV)">
      <g
         id="g12"
         inkscape:label="ACK / UNACK"
         transform="matrix(1.4446754,0,0,1.4446754,-7.3280069,-34.176006)">
        <rect
           style="fill:#b3b3b3;fill-opacity:1;stroke:none;stroke-width:2.91841;stroke-opacity:0.901961"
           id="rect19-8"
           width="4.5619702"
           height="4.5619702"
           x="54.855743"
           y="-9.4441338"
           transform="rotate(45)" />
        <text
           xml:space="preserve"
           style="font-size:2.64583px;line-height:0;font-family:Sans;-inkscape-font-specification:'Sans, Normal';word-spacing:0px;display:inline;fill:#1a1a1a;fill-opacity:1;stroke:none;stroke-width:3.17"
           x="44.411308"
           y="36.432377"
           id="text11"
           inkscape:label="H"><tspan
             sodipodi:role="line"
             id="tspan11"
             style="fill:#1a1a1a;fill-opacity:1;stroke:none;stroke-width:3.17"
             x="44.411308"
             y="36.432377">M</tspan></text>
      </g>
      <g
         id="g13"
         inkscape:label="Ban"
         transform="translate(0.72313976,-17.837447)"
         style="stroke-width:0.346344;stroke-dasharray:none">
        <circle
           cx="57.767361"
           cy="34.484028"
           r="3.5277777"
           id="circle12"
           style="fill:none;stroke:#000000;stroke-width:0.346344;stroke-dasharray:none;stroke-opacity:1" />
        <path
           d="m 55.262639,31.979305 5.009444,5.009444"
           id="path12"
           style="fill:none;fill-opacity:0.0192307;stroke:#000000;stroke-width:0.346344;stroke-dasharray:none;stroke-opacity:1" />
      </g>
    </g>
  </g>
</svg>
`
const OOSRV_LOW = `
<svg
   width="81"
   height="81"
   viewBox="0 0 23.279014 23.279013"
   version="1.1"
   id="svg1"
   sodipodi:docname="drawing.svg"
   inkscape:version="1.3.2 (091e20e, 2023-11-25, custom)"
   xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"
   xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"
   xmlns="http://www.w3.org/2000/svg"
   xmlns:svg="http://www.w3.org/2000/svg">
  <sodipodi:namedview
     id="namedview1"
     pagecolor="#ffffff"
     bordercolor="#000000"
     borderopacity="0.25"
     inkscape:showpageshadow="2"
     inkscape:pageopacity="0.0"
     inkscape:pagecheckerboard="0"
     inkscape:deskcolor="#d1d1d1"
     inkscape:document-units="mm"
     inkscape:zoom="0.71446409"
     inkscape:cx="396.80091"
     inkscape:cy="449.2878"
     inkscape:window-width="1708"
     inkscape:window-height="871"
     inkscape:window-x="2391"
     inkscape:window-y="-9"
     inkscape:window-maximized="1"
     inkscape:current-layer="layer1" />
  <defs
     id="defs1" />
  <g
     inkscape:label="Layer 1"
     inkscape:groupmode="layer"
     id="layer1">
    <g
       id="g22"
       transform="matrix(-2.4893904,0,0,-2.4893904,157.08545,52.946808)"
       style="display:inline;opacity:0.8"
       inkscape:label="Out of Service (OOSERV)">
      <g
         id="g20"
         inkscape:label="ACK / UNACK"
         transform="matrix(1.4446754,0,0,1.4446754,-7.3280069,-34.176006)">
        <path
           id="path20"
           style="fill:#b3b3b3;fill-opacity:1;stroke:#c62828;stroke-width:0;stroke-linejoin:bevel;stroke-opacity:0.901961"
           d="m 42.597917,37.205952 2.899532,-4.662202 2.934545,4.653532 z"
           sodipodi:nodetypes="cccc"
           inkscape:label="rect1-6" />
        <text
           xml:space="preserve"
           style="font-size:2.64583px;line-height:0;font-family:Sans;-inkscape-font-specification:'Sans, Normal';word-spacing:0px;display:inline;fill:#1a1a1a;fill-opacity:1;stroke:none;stroke-width:3.17"
           x="-46.350227"
           y="-34.508724"
           id="text20"
           inkscape:label="L"
           transform="scale(-1)"><tspan
             sodipodi:role="line"
             id="tspan20"
             style="fill:#1a1a1a;fill-opacity:1;stroke:none;stroke-width:3.17"
             x="-46.350227"
             y="-34.508724">L</tspan></text>
      </g>
      <g
         id="g21"
         inkscape:label="Ban"
         transform="translate(0.72313976,-17.837447)"
         style="stroke-width:0.346344;stroke-dasharray:none">
        <circle
           cx="57.767361"
           cy="34.484028"
           r="3.5277777"
           id="circle20"
           style="fill:none;stroke:#000000;stroke-width:0.346344;stroke-dasharray:none;stroke-opacity:1" />
        <path
           d="m 55.262639,31.979305 5.009444,5.009444"
           id="path21"
           style="fill:none;fill-opacity:0.0192307;stroke:#000000;stroke-width:0.346344;stroke-dasharray:none;stroke-opacity:1" />
      </g>
    </g>
  </g>
</svg>
`
// This function return icon for SHLVD, DPUPR & ERROR alarm status
const getLowAlarmSvg = (status) => {
   const pauseIcon = "SHLVD" === status ? `
      <text
         xml:space="preserve"
         style="font-size:3.45376px;line-height:0;font-family:Sans;-inkscape-font-specification:'Sans, Normal';word-spacing:0px;display:inline;fill:#000000;fill-opacity:0.996503;stroke:none;stroke-width:4.13797"
         x="38.633461"
         y="-44.375137"
         id="text25"
         transform="matrix(0,0.9111976,-1.0974568,0,0,0)"
         inkscape:label="Pause"><tspan
           sodipodi:role="line"
           id="tspan25"
           style="fill:#000000;fill-opacity:0.996503;stroke:none;stroke-width:4.13797"
           x="38.633461"
           y="-44.375137">=</tspan></text>

      ` : ``
   return `
   <svg
      width="81"
      height="81"
      viewBox="0 0 23.279014 23.279013"
      version="1.1"
      id="svg1"
      sodipodi:docname="drawing.svg"
      inkscape:version="1.3.2 (091e20e, 2023-11-25, custom)"
      xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"
      xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"
      xmlns="http://www.w3.org/2000/svg"
      xmlns:svg="http://www.w3.org/2000/svg">
     <sodipodi:namedview
        id="namedview1"
        pagecolor="#ffffff"
        bordercolor="#000000"
        borderopacity="0.25"
        inkscape:showpageshadow="2"
        inkscape:pageopacity="0.0"
        inkscape:pagecheckerboard="0"
        inkscape:deskcolor="#d1d1d1"
        inkscape:document-units="mm"
        inkscape:zoom="0.71446409"
        inkscape:cx="396.80091"
        inkscape:cy="449.2878"
        inkscape:window-width="1708"
        inkscape:window-height="871"
        inkscape:window-x="2391"
        inkscape:window-y="-9"
        inkscape:window-maximized="1"
        inkscape:current-layer="layer1" />
     <defs
        id="defs1" />
     <g
        inkscape:label="Layer 1"
        inkscape:groupmode="layer"
        id="layer1">
       <g
          id="g26"
          inkscape:label="Shelved (SHLVD)"
          transform="matrix(-3.596361,0,0,-3.596361,199.0353,137.34403)"
          style="display:inline;opacity:0.8">
         <g
            id="g25"
            inkscape:label="ACK / UNACK"
            transform="translate(6.592103,-0.03890271)">
           <path
              id="path24"
              style="fill:#b3b3b3;fill-opacity:1;stroke:#c62828;stroke-width:0;stroke-linejoin:bevel;stroke-opacity:0.901961"
              d="m 42.597917,37.205952 2.899532,-4.662202 2.934545,4.653532 z"
              sodipodi:nodetypes="cccc"
              inkscape:label="rect1-6" />
           <text
              xml:space="preserve"
              style="font-size:2.64583px;line-height:0;font-family:Sans;-inkscape-font-specification:'Sans, Normal';word-spacing:0px;display:inline;fill:#1a1a1a;fill-opacity:1;stroke:none;stroke-width:3.17"
              x="-46.230423"
              y="-34.508724"
              id="text24"
              inkscape:label="L"
              transform="scale(-1)"><tspan
                sodipodi:role="line"
                id="tspan24"
                style="fill:#1a1a1a;fill-opacity:1;stroke:none;stroke-width:3.17"
                x="-46.230423"
                y="-34.508724">L</tspan></text>
         </g>
         ${pauseIcon}
       </g>
     </g>
   </svg>
   `
}
const getMidAlarmSvg = (status) => {
   const pauseIcon = ALARM_STATUS_SHLVD === status ? `
      <text
         xml:space="preserve"
         style="font-size:3.45376px;line-height:0;font-family:Sans;-inkscape-font-specification:'Sans, Normal';word-spacing:0px;display:inline;fill:#000000;fill-opacity:0.996503;stroke:none;stroke-width:4.13797"
         x="38.633461"
         y="-44.375137"
         id="text25"
         transform="matrix(0,0.9111976,-1.0974568,0,0,0)"
         inkscape:label="Pause"><tspan
           sodipodi:role="line"
           id="tspan25"
           style="fill:#000000;fill-opacity:0.996503;stroke:none;stroke-width:4.13797"
           x="38.633461"
           y="-44.375137">=</tspan></text>

      ` : ``
   return `
<svg
   width="81"
   height="81"
   viewBox="0 0 23.279014 23.279013"
   version="1.1"
   id="svg1"
   sodipodi:docname="drawing.svg"
   inkscape:version="1.3.2 (091e20e, 2023-11-25, custom)"
   xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"
   xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"
   xmlns="http://www.w3.org/2000/svg"
   xmlns:svg="http://www.w3.org/2000/svg">
  <sodipodi:namedview
     id="namedview1"
     pagecolor="#ffffff"
     bordercolor="#000000"
     borderopacity="0.25"
     inkscape:showpageshadow="2"
     inkscape:pageopacity="0.0"
     inkscape:pagecheckerboard="0"
     inkscape:deskcolor="#d1d1d1"
     inkscape:document-units="mm"
     inkscape:zoom="0.71446409"
     inkscape:cx="396.80091"
     inkscape:cy="449.2878"
     inkscape:window-width="1708"
     inkscape:window-height="871"
     inkscape:window-x="2391"
     inkscape:window-y="-9"
     inkscape:window-maximized="1"
     inkscape:current-layer="layer1" />
  <defs
     id="defs1" />
  <g
     inkscape:label="Layer 1"
     inkscape:groupmode="layer"
     id="layer1">
    <g
       id="g18"
       inkscape:label="Shelved (SHLVD)"
       transform="matrix(3.596361,0,0,3.596361,-175.56156,-115.61243)"
       style="opacity:0.8">
      <g
         id="g17"
         inkscape:label="ACK / UNACK"
         transform="translate(6.592103,-0.03890271)">
        <rect
           style="fill:#b3b3b3;fill-opacity:1;stroke:none;stroke-width:2.91841;stroke-opacity:0.901961"
           id="rect19-83"
           width="4.5619702"
           height="4.5619702"
           x="54.912102"
           y="-9.3791933"
           transform="rotate(45)"
           inkscape:label="rect19-83" />
        <text
           xml:space="preserve"
           style="font-size:2.64583px;line-height:0;font-family:Sans;-inkscape-font-specification:'Sans, Normal';word-spacing:0px;display:inline;fill:#1a1a1a;fill-opacity:1;stroke:none;stroke-width:3.17"
           x="44.382854"
           y="36.432377"
           id="text16"
           inkscape:label="M"><tspan
             sodipodi:role="line"
             id="tspan16"
             style="fill:#1a1a1a;fill-opacity:1;stroke:none;stroke-width:3.17"
             x="44.382854"
             y="36.432377">M</tspan></text>
      </g>
      ${pauseIcon}
    </g>
  </g>
</svg>
`
}
const getHighAlarmSvg = (status) => {
   const pauseIcon = "SHLVD" === status ? `
      <text
         xml:space="preserve"
         style="font-size:3.45376px;line-height:0;font-family:Sans;-inkscape-font-specification:'Sans, Normal';word-spacing:0px;display:inline;fill:#000000;fill-opacity:0.996503;stroke:none;stroke-width:4.13797"
         x="38.633461"
         y="-44.375137"
         id="text25"
         transform="matrix(0,0.9111976,-1.0974568,0,0,0)"
         inkscape:label="Pause"><tspan
           sodipodi:role="line"
           id="tspan25"
           style="fill:#000000;fill-opacity:0.996503;stroke:none;stroke-width:4.13797"
           x="38.633461"
           y="-44.375137">=</tspan></text>

      ` : ``
   return `
<svg
   width="81"
   height="81"
   viewBox="0 0 23.279014 23.279013"
   version="1.1"
   id="svg1"
   sodipodi:docname="drawing.svg"
   inkscape:version="1.3.2 (091e20e, 2023-11-25, custom)"
   xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"
   xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"
   xmlns="http://www.w3.org/2000/svg"
   xmlns:svg="http://www.w3.org/2000/svg">
  <sodipodi:namedview
     id="namedview1"
     pagecolor="#ffffff"
     bordercolor="#000000"
     borderopacity="0.25"
     inkscape:showpageshadow="2"
     inkscape:pageopacity="0.0"
     inkscape:pagecheckerboard="0"
     inkscape:deskcolor="#d1d1d1"
     inkscape:document-units="mm"
     inkscape:zoom="0.71446409"
     inkscape:cx="396.80091"
     inkscape:cy="449.2878"
     inkscape:window-width="1708"
     inkscape:window-height="871"
     inkscape:window-x="2391"
     inkscape:window-y="-9"
     inkscape:window-maximized="1"
     inkscape:current-layer="layer1" />
  <defs
     id="defs1" />
  <g
     inkscape:label="Layer 1"
     inkscape:groupmode="layer"
     id="layer1">
    <g
       id="g11"
       inkscape:label="Shelved (SHLVD)"
       transform="matrix(3.596361,0,0,3.596361,-175.46809,-114.02521)"
       style="display:inline">
      <g
         id="g9-9-1"
         inkscape:label="ACK / UNACK"
         transform="translate(6.592103,-0.03890271)"
         style="opacity:0.8;fill:#b3b3b3">
        <path
           id="rect1-6-0-8"
           style="fill:#b3b3b3;fill-opacity:1;stroke:#c62828;stroke-width:0;stroke-linejoin:bevel;stroke-opacity:0.901961"
           d="m 42.597917,37.205952 2.899532,-4.662202 2.934545,4.653532 z"
           sodipodi:nodetypes="cccc"
           inkscape:label="rect1-6" />
        <text
           xml:space="preserve"
           style="font-size:2.64583px;line-height:0;font-family:Sans;-inkscape-font-specification:'Sans, Normal';word-spacing:0px;display:inline;fill:#1a1a1a;fill-opacity:1;stroke:none;stroke-width:3.17"
           x="44.496681"
           y="36.432377"
           id="text1-1-9-5"
           inkscape:label="H"><tspan
             sodipodi:role="line"
             id="tspan1-5-3-8"
             style="fill:#1a1a1a;fill-opacity:1;stroke:none;stroke-width:3.17"
             x="44.496681"
             y="36.432377">H</tspan></text>
      </g>
      ${pauseIcon}
   </g>
  </g>
</svg>
   `
}

const highPriorityAlarms = {
   'ACK': ACK_HIGH,
   'UNACK': ACK_HIGH,
   'RTNUN': ACK_HIGH,
   'OOSRV': OOSRV_HIGH,
   'SHLVD': getHighAlarmSvg("SHLVD"),
   'DPUPR': getHighAlarmSvg("DPUPR"),
   'ERROR': getHighAlarmSvg("ERROR"),
}
const mediumPriorityAlarms = {
   'ACK': ACK_MED,
   'UNACK': ACK_MED,
   'RTNUN': ACK_MED,
   'OOSRV': OOSRV_MED,
   'SHLVD': getMidAlarmSvg("SHLVD"),
   'DPUPR': getMidAlarmSvg("DPUPR"),
   'ERROR': getMidAlarmSvg("ERROR"),
}
const lowPriorityAlarms = {
   'ACK': ACK_LOW,
   'UNACK': ACK_LOW,
   'RTNUN': ACK_LOW,
   'OOSRV': OOSRV_LOW,
   'SHLVD': getLowAlarmSvg("SHLVD"),
   'DPUPR': getLowAlarmSvg("DPUPR"),
   'ERROR': getLowAlarmSvg("ERROR"),
}