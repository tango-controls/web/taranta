import React, { Component, CSSProperties } from "react";
import { WidgetProps } from "../types";

import {
    AttributeInputDefinition,
    ColorInputDefinition,
    NumberInputDefinition,
    SelectInputDefinition,
    StyleInputDefinition,
    WidgetDefinition,
} from "../../types";

import "../styles/AlarmIndicator.styles.css";
import AlarmIndicatorValues from "./AlarmIndicatorValues";
import { parseCss } from "../../components/Inspector/StyleSelector";

type Inputs = {
    attribute: AttributeInputDefinition;
    widgetCss: StyleInputDefinition;
    textColor: ColorInputDefinition;
    backgroundColor: ColorInputDefinition;
    size: NumberInputDefinition;
    font: SelectInputDefinition;
};

type Props = WidgetProps<Inputs>;

class AlarmIndicator extends Component<Props> {
    public render() {
        const { mode } = this.props;
        const { device } = this.deviceAndAttribute();
        const {
            attribute,
            textColor,
            size,
            backgroundColor,
        } = this.props.inputs;

        const widgetCss = this.props.inputs.widgetCss
            ? parseCss(this.props.inputs.widgetCss).data
            : {};
        const style: CSSProperties = {
            backgroundColor,
            color: textColor,
            fontSize: size + "em",
            height: "library" === mode ? "60px" : "100%",
            width: "library" === mode ? "160px" : "100%",
            ...widgetCss,
        };

        if (mode === "library") {
            return (
                <div id="AlarmIndicatorLib" style={style} className="justify-content-left alarm-indicator">
                </div>
            );
        }

        return (
            <div id="AlarmIndicator" style={style}>
                <AlarmIndicatorValues
                    attributeName={attribute?.attribute}
                    deviceName={device}
                    enumlabels={attribute?.enumlabels}
                    mode={mode}
                />
            </div>
        );
    }

    private deviceAndAttribute(): {
        device: string;
        name: string;
        label: string;
    } {
        const { attribute } = this.props.inputs;
        const device = attribute.device || "device";
        const name = attribute.attribute || "attributeName";
        const label = attribute.label || "attributeLabel";
        return { device, name, label };
    }
}

const definition: WidgetDefinition<Inputs> = {
    type: "ALARM_INDICATOR",
    name: "Alarm Indicator",
    defaultWidth: 5,
    defaultHeight: 5,
    inputs: {
        attribute: {
            type: "attribute",
            label: "",
            dataFormat: "alarm",
            required: true,
        },
        textColor: {
            label: "Text Color",
            type: "color",
            default: "#000000",
        },
        backgroundColor: {
            label: "Background Color",
            type: "color",
            default: "#ffffff",
        },
        size: {
            label: "Text size (in units)",
            type: "number",
            default: 1,
            nonNegative: true,
        },
        font: {
            type: "select",
            default: "Helvetica",
            label: "Font type",
            options: [
                {
                    name: "Default (Helvetica)",
                    value: "Helvetica",
                },
                {
                    name: "Monospaced (Courier new)",
                    value: "Courier new",
                },
            ],
        },
        widgetCss: {
            type: "style",
            label: "Custom CSS",
            default: "",
        },
    },
};

const AlarmIndicatorExport = { component: AlarmIndicator, definition };
export default AlarmIndicatorExport;
