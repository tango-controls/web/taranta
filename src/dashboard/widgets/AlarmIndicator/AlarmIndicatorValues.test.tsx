import React from "react";
import { render } from "@testing-library/react";
import { useSelector as useSelectorMock } from "react-redux";
import AlarmIndicatorValues from "./AlarmIndicatorValues";
import { ALARM_ENUM_LABELS } from "./AlarmConst";

jest.mock("react-redux", () => ({
  useSelector: jest.fn(),
}));

const useSelector = useSelectorMock as jest.Mock;

describe("AlarmIndicatorValues", () => {
  beforeEach(() => {
    useSelector.mockImplementation((selectorFn: any) =>
      selectorFn({
        messages: {
          "testdb://alarm/taranta/01": {
            "attributes": {
              "taranta_logalarm": {
                "values": [1],
                "quality": ["ATTR_VALID"],
                "timestamp": [1731654621.08356]
              },
              "taranta_warningalarm": {
                "values": [3],
                "quality": ["ATTR_VALID"],
                "timestamp": [1731654621.08356]
              },
              "taranta_faultalarm": {
                "values": [3],
                "quality": ["ATTR_VALID"],
                "timestamp": [1731654621.08356]
              },
              "alarmSummary": {
                "values": [
                  [
                    'tag=taranta_faultalarm;state=RTNUN;priority=fault;time=2024-11-15 05:12:49.170972;formula=(test/tarantatestdevice/1/alarmSimulator == 4);message="alarmSimulator fault event"',
                    'tag=taranta_logalarm;state=UNACK;priority=log;time=2024-11-15 05:12:56.238372;formula=((test/tarantatestdevice/1/alarmSimulator >= 0) && (test/tarantatestdevice/1/alarmSimulator < 2));message="alarmSimulator log event"',
                    'tag=taranta_warningalarm;state=RTNUN;priority=warning;time=2024-11-15 05:12:56.238372;formula=((test/tarantatestdevice/1/alarmSimulator >= 2) && (test/tarantatestdevice/1/alarmSimulator < 4));message="alarmSimulator warning event"'
                  ],
                ],
                "quality": ["ATTR_VALID"],
                "timestamp": [1731654621.08356]
              },
              "alarmsummary": {
                "values": [
                  [
                    'tag=taranta_faultalarm;state=RTNUN;priority=fault;time=2024-11-15 05:12:49.170972;formula=(test/tarantatestdevice/1/alarmSimulator == 4);message="alarmSimulator fault event"',
                    'tag=taranta_logalarm;state=UNACK;priority=log;time=2024-11-15 05:12:56.238372;formula=((test/tarantatestdevice/1/alarmSimulator >= 0) && (test/tarantatestdevice/1/alarmSimulator < 2));message="alarmSimulator log event"',
                    'tag=taranta_warningalarm;state=RTNUN;priority=warning;time=2024-11-15 05:12:56.238372;formula=((test/tarantatestdevice/1/alarmSimulator >= 2) && (test/tarantatestdevice/1/alarmSimulator < 4));message="alarmSimulator warning event"'
                  ]
                ],
                "quality": ["ATTR_VALID"],
                "timestamp": [1731654621.08356]
              }
            }
          }
        },
        ui: {
          mode: "run",
        },
      })
    );
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  it("should render fault priority alarm", () => {
    const { container } = render(
      <AlarmIndicatorValues
        deviceName="testdb://alarm/taranta/01"
        attributeName="taranta_faultalarm"
        enumlabels={ALARM_ENUM_LABELS}
        mode="run"
      />
    );
    expect(container.textContent).toContain("H");
    const title = "High priority: " + ALARM_ENUM_LABELS?.[3] + " - 1731654621.08356 - ATTR_VALID";
    expect(container.innerHTML).toContain(title);
    expect(container.innerHTML).toContain("svg-container opacity-50");
    

  });

  it("should render log priority alarm", () => {
    const { container } = render(
      <AlarmIndicatorValues
        deviceName="testdb://alarm/taranta/01"
        attributeName="taranta_logalarm"
        enumlabels={ALARM_ENUM_LABELS}
        mode="run"
      />
    );
    expect(container.textContent).toContain("L");
    expect(container.innerHTML).toContain("svg-container blinking");
  });

  it("should render warning priority alarm", () => {
    const { container } = render(
      <AlarmIndicatorValues
        deviceName="testdb://alarm/taranta/01"
        attributeName="taranta_warningalarm"
        enumlabels={ALARM_ENUM_LABELS}
        mode="run"
      />
    );
    expect(container.textContent).toContain("M");
  });
});
