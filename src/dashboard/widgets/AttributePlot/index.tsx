import AttributePlot, {Inputs} from "./AttributePlot";
import {
  WidgetDefinition,
} from "../../../dashboard/types";

const definition: WidgetDefinition<Inputs> = {
  type: "ATTRIBUTE_PLOT",
  name: "Attribute Plot",
  defaultWidth: 30,
  defaultHeight: 20,
  inputs: {
    timeWindow: {
      type: "number",
      default: 120,
      label: "Time Window"
    },
    attributes: {
      label: "Graphs",
      type: "complex",
      repeat: true,
      inputs: {
        attribute: {
          label: "",
          type: "attribute",
          required: true,
          dataFormat: "scalar",
          dataType: "numeric"
        },
        showAttribute: {
          type: "select",
          label: "Attribute display:",
          default: "Label",
          options: [
            {
              name: "Label",
              value: "Label"
            },
            {
              name: "Name",
              value: "Name"
            }
          ]
        },
        yAxis: {
          type: "select",
          default: "left",
          label: "Y Axis",
          options: [
            {
              name: "Left",
              value: "left"
            },
            {
              name: "Right",
              value: "right"
            }
          ]
        },
        lineColor: {
          label: "Line Color",
          type: "color",
          default: "#000000"
        },
      }
    },
    showZeroLine: {
      type: "boolean",
      default: true,
      label: "Show Zero Line"
    },
    xScientificNotation: {
      type: "boolean",
      label: "X-axis Scientific Notation",
      default: false
    },
    yScientificNotation: {
      type: "boolean",
      label: "Y-axis Scientific Notation",
      default: false
    },
    xLogarithmicScale: {
      type: "boolean",
      default: false,
      label: "X-axis Logarithmic Scale"
    },
    yLogarithmicScale: {
      type: "boolean",
      default: false,
      label: "Y-axis Logarithmic Scale"
    },
    showTangoDB: {
      type: "boolean",
      label: "Show Tango database name",
      default: false,
    },
    textColor: {
      label: "Text Color",
      type: "color",
      default: "#000000"
    },
    backgroundColor: {
      label: "Background Color",
      type: "color",
      default: "#ffffff"
    }
  }
};

const AttributePlotExport = { definition, component: AttributePlot };
export default AttributePlotExport;