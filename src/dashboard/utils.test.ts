import { fetchAttributeMetadata, isEditableDashboard } from "./utils";
import { SelectedDashboardState } from "../shared/state/reducers/selectedDashboard";
import { IUserState } from "../shared/user/state/reducer";
import tangoAPI from '../shared/api/tangoAPI';

describe("isEditableDashboard", () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  jest.mock("../shared/state/reducers/selectedDashboard", () => ({
    SelectedDashboardState: {
      group: "group1",
      groupWriteAccess: true,
      user: "testUser",
      widgets: {},
    },
  }));

  jest.mock("../shared/user/state/reducer", () => ({
    IUserState: {
      username: "testUser",
      userGroups: ["group2", "group3"],
    },
  }));

  it("should return true if the user is the owner", () => {
    const selectedDashboardMock: SelectedDashboardState = require("../shared/state/reducers/selectedDashboard")
      .SelectedDashboardState;
    const currentUserMock: IUserState = require("../shared/user/state/reducer")
      .IUserState;

    const result = isEditableDashboard(selectedDashboardMock, currentUserMock);
    expect(result.result).toBe(true);
  });

  it("should return true if the user is in the same group and has write access", () => {
    const selectedDashboardMock: SelectedDashboardState = require("../shared/state/reducers/selectedDashboard")
      .SelectedDashboardState;
    const currentUserMock: IUserState = require("../shared/user/state/reducer")
      .IUserState;
    currentUserMock.username = "OtherUser";
    currentUserMock.userGroups?.push("group1");

    const result = isEditableDashboard(selectedDashboardMock, currentUserMock);
    expect(result.result).toBe(true);
  });

  it("should return false if the user is not logged in", () => {
    const selectedDashboardMock: SelectedDashboardState = require("../shared/state/reducers/selectedDashboard")
      .SelectedDashboardState;
    const currentUserMock: IUserState = require("../shared/user/state/reducer")
      .IUserState;
    currentUserMock.username = undefined;

    const result = isEditableDashboard(selectedDashboardMock, currentUserMock);
    expect(result.result).toBe(false);
  });

  it("should return false if the user is in the same group and has no write access", () => {
    const selectedDashboardMock: SelectedDashboardState = require("../shared/state/reducers/selectedDashboard")
      .SelectedDashboardState;
    const currentUserMock: IUserState = require("../shared/user/state/reducer")
      .IUserState;
    currentUserMock.username = "otherUser";
    currentUserMock.userGroups?.push("group1");

    let result = isEditableDashboard(selectedDashboardMock, currentUserMock);
    expect(result.result).toBe(true);

    selectedDashboardMock.groupWriteAccess = false; //change user permission
    result = isEditableDashboard(selectedDashboardMock, currentUserMock);
    expect(result.result).toBe(false);
  });
});

describe("isEditableDashboard for admin user", () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  jest.mock("../shared/state/reducers/selectedDashboard", () => ({
    SelectedDashboardState: {
      group: "group1",
      groupWriteAccess: true,
      user: "testUser",
      widgets: {},
    },
  }));

  jest.mock("../shared/user/state/reducer", () => ({
    IUserState: {
      username: "admin",
      userGroups: ["group1", "group2", "group3"],
    },
  }));


  it("should return true if the user is the admin", () => {
    window['config']['ADMIN_USERNAME'] = 'admin';

    const selectedDashboardMock: SelectedDashboardState = require("../shared/state/reducers/selectedDashboard")
      .SelectedDashboardState;
    const currentUserMock: IUserState = require("../shared/user/state/reducer")
      .IUserState;

    currentUserMock.username = 'admin';
    const result = isEditableDashboard(selectedDashboardMock, currentUserMock);
    expect(result.result).toBe(true);
  });

  it("test fetchAttributeMetadata", async() => {
    const mockRes = {
      "testdb://test/tarantatestdevice/1/dishstate": {
          "dataFormat": "scalar",
          "dataType": "DevEnum",
          "unit": "",
          "enumlabels": [
              "Standby",
              "Ready",
              "Slew",
              "Track",
              "Scan",
              "Stow",
              "Error"
          ],
          "label": "DishState",
          "maxAlarm": null,
          "minAlarm": null,
          "minValue": null,
          "maxValue": null
      },
      "testdb://test/tarantatestdevice/1/cspobsstate": {
          "dataFormat": "scalar",
          "dataType": "DevEnum",
          "unit": "",
          "enumlabels": [
              "Empty",
              "Resourcing",
              "Idle",
              "Configuring",
              "Ready",
              "Scanning",
              "Aborting",
              "Aborted",
              "Resetting",
              "Fault",
              "Restarting"
          ],
          "label": "CspObsState",
          "maxAlarm": null,
          "minAlarm": null,
          "minValue": null,
          "maxValue": null
      }
    }

    jest.spyOn(tangoAPI, "fetchAttributeMetadata").mockResolvedValue(mockRes)

    const fullNames = ["testdb://test/tarantatestdevice/1/dishstate", "testdb://test/tarantatestdevice/1/cspobsstate"]
    const result = await fetchAttributeMetadata(fullNames);
    expect(result).toEqual(mockRes);
  })

});
