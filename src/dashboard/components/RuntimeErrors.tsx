import React from "react";

export interface RuntimeErrorDescriptor {
  type: "warning" | "error";
  message: string;
}

export default function RuntimeErrors(props: {
  errors: RuntimeErrorDescriptor[];
}) {
  let { errors } = props;
  errors = errors.filter((error) => error.message !== "");

  if (errors.length === 0) {
    return null;
  }

  return errors.length === 0 ? null : <div className="RuntimeErrors"> </div>;
}
