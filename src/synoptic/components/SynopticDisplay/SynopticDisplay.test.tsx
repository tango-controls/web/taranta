import React from "react";
import { render, waitFor } from "@testing-library/react";
import { useSelector as useSelectorMock } from "react-redux";
import { useDispatch as useDispatchMock } from "react-redux";
import SynopticDisplay from "./SynopticDisplay";
import { WEBSOCKET } from "../../../shared/state/actions/actionTypes";

jest.mock("react-redux", () => ({
  useSelector: jest.fn(),
  useDispatch: jest.fn(),
  connect: () => (Component: any) => Component,
}));
const useSelector = useSelectorMock as jest.Mock;

const fs = require("fs");
const svgFile = fs
  .readFileSync("src/synoptic/testingUtils/testSVGfile.svg")
  .toString();
let myFile = {
  svg: svgFile,
  selectedId: null,
  selectedIds: [],
  id: "",
  name: "Untitled synoptic",
  user: "",
  group: "",
  groupWriteAccess: false,
  lastUpdatedBy: "",
  insertTime: null,
  updateTime: null,
  history: {
    undoActions: [],
    redoActions: [],
    undoIndex: 0,
    redoIndex: 0,
    undoLength: 0,
    redoLength: 0,
  },
  variables: [],
};
const useDispatch = useDispatchMock as jest.Mock;
describe("SynopticDisplay", () => {
  const dispatch = jest.fn();
  beforeEach(() => {
    useSelector.mockReturnValue(myFile);
    useDispatch.mockReturnValue(dispatch);
  });
  afterEach(() => {
    jest.resetAllMocks();
  });

  it("should render the synoptic display", () => {
    const { container } = render(<SynopticDisplay mode="edit" />);
    expect(container.innerHTML).toContain("SvgComponent");
    expect(container.innerHTML).toContain("thumbnail");
  });

  it("subscribe to tango devices in run mode", () => {
    waitFor(() => {
      const { container } = render(<SynopticDisplay mode="run" />);
      expect(dispatch).toHaveBeenCalledWith({
        type: WEBSOCKET.WS_SUBSCRIBE,
        payload: {
          devices: ["://sys/database/2/state", "://sys/tg_test/1/state"],
        },
      });
      expect(container.innerHTML).toContain("SvgComponent");
    });
  });

  it("add viewbox for missing synoptic", () => {
    const svgFileNoViewbox = svgFile.replace(
      'viewBox="0 0 407.90144 229.10447"',
      ""
    );
    myFile = {
      svg: svgFileNoViewbox,
      selectedId: null,
      selectedIds: [],
      id: "",
      name: "Untitled synoptic",
      user: "",
      group: "",
      groupWriteAccess: false,
      lastUpdatedBy: "",
      insertTime: null,
      updateTime: null,
      history: {
        undoActions: [],
        redoActions: [],
        undoIndex: 0,
        redoIndex: 0,
        undoLength: 0,
        redoLength: 0,
      },
      variables: [],
    };
    useSelector.mockReturnValue(myFile);
    const { container } = render(<SynopticDisplay mode="edit" />);
    // expect the vieBox to be set to the width and height value in the svg file
    expect(container.innerHTML).toContain('viewBox="0 0 425.61797 250.36431"');
  });
});
