import React from "react";
import "./TopBar.css";
import ModeToggleButton from "./ModeToggleButton/ModeToggleButton";
import SynopticTitle from "./SynopticTitle/SynopticTitle";
import { Navbar } from "../../shared/ui/navbar/Navbar";

interface Props {
  mode: "edit" | "run";
  onToggleMode: () => void;
  modeToggleDisabled: boolean;
}

export default function TopBar(props: Props) {
  const { mode, onToggleMode, modeToggleDisabled } = props;

  return (
    <Navbar>
      <div className="topbar_synoptic">
        <form className="form-inline" style={{ display: "inline-block" }}>
          <ModeToggleButton
            onClick={onToggleMode}
            disabled={modeToggleDisabled}
            mode={mode}
          />
        </form>
        <SynopticTitle />
      </div>
    </Navbar>
  );
}
