import reducer from "./selectedSynoptic";
import { synopticRenamed } from "../actionCreators";

const basicState = {
  svg: "",
  selectedId: null,
  selectedIds: [],
  id: "",
  name: "",
  user: "",
  group: "",
  lastUpdatedBy: "",
  insertTime: null,
  updateTime: null,
  groupWriteAccess: true,
  history: {
    undoActions: [],
    redoActions: [],
    undoIndex: 0,
    redoIndex: 0,
    undoLength: 0,
    redoLength: 0,
  },
  variables: [
    {
      _id: "0698hln1j359a",
      name: "myvar",
      class: "tg_test",
      device: "sys/tg_test/1",
    },
  ],
};

const savedState = {
  ...basicState,
  id: "5cb49ad146f4c024ece1cea5",
  insertTime: new Date("1990"),
  updateTime: new Date("2000"),
};

test("SYNOPTIC_RENAMED", () => {
  const newName = "new name";
  const action = synopticRenamed(savedState.id, newName);

  const state = reducer(savedState, action);
  expect(state.name).toEqual(newName);
});
