import { request as graphqlRequest } from "graphql-request";
import MongoAPI from "./mongoAPI";
import {
  FETCH_ATTRIBUTES,
  FETCH_COMMANDS,
  FETCH_DEVICE_NAMES,
  FETCH_DEVICE_SERVER_NAMES,
  FETCH_DEVICE_STATE,
  EXECUTE_COMMAND,
  SET_DEVICE_ATTRIBUTE,
  SET_DEVICE_PROPERTY,
  DELETE_DEVICE_PROPERTY,
  FETCH_ATTRIBUTE_METADATA,
  FETCH_ATTRIBUTES_VALUES,
  FETCH_DEVICE_METADATA,
  FETCH_DEVICE,
  FETCH_DATABASE_INFO,
  FETCH_COMMUNICATION_HEALTH,
  FETCH_ALL_CLASSES_WITH_DEVICES,
  FETCH_CLASS_WITH_DEVICES,
  FETCH_ALL_CLASSES,
  FETCH_COMMUNICATION_HEALTH_NEW,
  FETCH_TANGO_HOST,
} from "./graphqlQuery";

import {
  FetchAttributes,
  FetchCommands,
  FetchDeviceNames,
  FetchDeviceServerNames,
} from "./graphqlQuery";

import { Variables } from "graphql-request/src/types";

import sanitize_device_name from "../../shared/utils/SanitizeDeviceName";
import {
  createDeviceWithTangoDBFullPath,
  getDeviceFromPath,
} from "../../dashboard/runtime/utils";
import { splitFullPath } from "../../dashboard/DBHelper";

const config = window["config"];

function request<T = any>(
  tangoDB: string,
  query: string,
  args?: Variables
): Promise<T> {
  const url = config.basename + `/${tangoDB}/db`;
  return graphqlRequest(url, query, args);
}

const tangoAPIExport = {
  async fetchDeviceAttributes(tangoDB: string, device: string[]) {
    try {
      const data = await request<FetchAttributes>(tangoDB, FETCH_ATTRIBUTES, {
        device,
      });
      const { attributes } = data.device;
      if (attributes != null) {
        // Put all attribute name to be lower case, old tangogql did that by default,
        // new tangogql-ariadne doesn't and can send attributes with upper case letters
        const updatedAttributes = attributes.map((attribute) => ({
          ...attribute, 
          name: attribute.name.toLocaleLowerCase(), 
          deviceName: device,
        }));
        return updatedAttributes;
      } else {
        // Some kind of error reporting could go here. This should only happen when the attributes resolver in the backend fails for some unexpected reason. For now, just return an empty list.
        return [];
      }
    } catch (err) {
      return [];
    }
  },

  async fetchDatabaseInfo(tangoDB: string) {
    try {
      const data = await request(tangoDB, FETCH_DATABASE_INFO);
      return data.info;
    } catch (err) {
      return String(err);
    }
  },

  async fetchTangoHost(tangoDB: string) {
    try {
      const data = await request(tangoDB, FETCH_TANGO_HOST);
      return data;
    } catch (err) {
      return String(err);
    }
  }
  ,

  async fetchCommunicationHealth(tangoDB: string) {
    try {
      const data = await request(tangoDB, FETCH_TANGO_HOST);
      const tangoHost = data.tangoHost;
      if(tangoHost && tangoHost !== "") {
      const query = "true" === sessionStorage.getItem('TANGOGQL_ARIADNE')
        ? FETCH_COMMUNICATION_HEALTH_NEW : FETCH_COMMUNICATION_HEALTH;
      const data = await request(tangoDB, query);
      return data;
      }

      throw new Error("Tango Host not found");
    } catch (err: any) {
      try {
        const metricError = "Cannot query field 'executionTime' on type 'Metrics'"
        if (err?.response?.status === 400 || err?.response?.errors.some(x => x.includes(metricError))) {
          const data = await request(tangoDB, FETCH_COMMUNICATION_HEALTH_NEW);
          sessionStorage.setItem("TANGOGQL_ARIADNE", "true");
          return data;
        }
      } catch (error) {
        return String(error)
      }
      return String(err);
    }
  },

  async fetchCommands(tangoDB: string, device: string) {
    try {
      const data = await request<FetchCommands>(tangoDB, FETCH_COMMANDS, {
        device,
      });
      return data.device.commands;
    } catch (err) {
      return [];
    }
  },

  async fetchDeviceNames(tangoDB: string) {
    try {
      const data = await request<FetchDeviceNames>(tangoDB, FETCH_DEVICE_NAMES);
      return data.devices.map(({ name }) => name.toLowerCase());
    } catch (err) {
      return [];
    }
  },

  async FetchDeviceServerNames(tangoDB: string) {
    let tangoHost = "";
    try {
      tangoHost = await request(tangoDB, FETCH_TANGO_HOST);
    } catch (err) {}
    try {
      if (tangoHost !== "") {
        const data = await request<FetchDeviceServerNames>(
          tangoDB,
          FETCH_DEVICE_SERVER_NAMES
        );
        return data.devices.map(function({ name, server }) {
          return { name: name.toLowerCase(), server };
        });
      } else {
        const data = await request<FetchDeviceNames>(tangoDB, FETCH_DEVICE_NAMES);
        return data.devices.map(function({ name }) {
          return { name: name.toLowerCase(), server: null };
        });
      }
    } catch (err) {
      return [];
    }
  },

  async executeCommand(
    tangoDB: string,
    device: string,
    command: string,
    argin?: any
  ) {
    try {
      const args =
        argin !== "" ? { device, command, argin } : { device, command };
      const data = await request(tangoDB, EXECUTE_COMMAND, args);
      const { ok, output, message } = data.executeCommand;
      if (ok) {
        const timestamp = new Date();
        MongoAPI.saveUserAction({
          actionType: "ExcuteCommandUserAction",
          timestamp,
          tangoDB,
          device,
          name: command,
          argin,
        });
      }
      return { ok, output, message };
    } catch (err) {
      return { ok: false, output: "", message: err };
    }
  },

  async setDeviceAttribute(
    tangoDB: string,
    device: string,
    name: string,
    value: any
  ) {
    try {
      const deviceName = getDeviceFromPath(device);
      const args = { device: deviceName, attribute: name, value };
      const data = await request(tangoDB, SET_DEVICE_ATTRIBUTE, args);
      const { ok, valueBefore, attribute } = data.setAttributeValue;
      if (ok) {
        const timestamp = new Date();
        MongoAPI.saveUserAction({
          actionType: "SetAttributeValueUserAction",
          timestamp,
          tangoDB,
          device,
          name: attribute.name,
          valueBefore,
          valueAfter: attribute.value,
          value,
        });
      }
      return { ok, attribute };
    } catch (err) {
      return { ok: false, attribute: null };
    }
  },

  async setDeviceProperty(
    tangoDB: string,
    device: string,
    name: string,
    value: any
  ) {
    let deviceName = getDeviceFromPath(device);
    const args = { device: deviceName, name, value };
    const data = await request(tangoDB, SET_DEVICE_PROPERTY, args);
    const { ok } = data.putDeviceProperty;
    if (ok) {
      const timestamp = new Date();
      MongoAPI.saveUserAction({
        actionType: "PutDevicePropertyUserAction",
        timestamp,
        tangoDB,
        device,
        name,
        value,
      });
    }
    return data.putDeviceProperty.ok;
  },

  async deleteDeviceProperty(tangoDB: string, device: string, name: string) {
    let deviceName = getDeviceFromPath(device);
    const args = { device: deviceName, name };
    const data = await request(tangoDB, DELETE_DEVICE_PROPERTY, args);
    const { ok } = data.deleteDeviceProperty;
    if (ok) {
      const timestamp = new Date();
      MongoAPI.saveUserAction({
        actionType: "DeleteDevicePropertyUserAction",
        timestamp,
        tangoDB,
        device,
        name,
      });
    }
    return ok;
  },

  async fetchDevice(tangoDB: string, name: string) {
    let [tangoDBActual, deviceName] = splitFullPath(name);
    tangoDBActual = tangoDBActual === "" ? tangoDB : tangoDBActual;
    const args = { name: deviceName };
    let device: any = null;
    let errors = [];

    try {
      const data = await request(tangoDBActual, FETCH_DEVICE, args);
      device = data.device;
      if (device === null) {
        return null;
      }
      data?.device?.attributes?.map((attribute) => {
        return attribute.name = attribute.name.toLocaleLowerCase();
      })
      return { ...device, name: createDeviceWithTangoDBFullPath(tangoDBActual, device.name.toLowerCase()), errors };
    } catch (err: any) {
      // The structure of errors is currently not ideal and will probably undergo change. Update this logic accordingly.
      errors = err.response.errors;
      device = err.response.data.device;
      if (device === null) {
        return null;
      }
      return { ...device, name: createDeviceWithTangoDBFullPath(tangoDBActual, device.name.toLowerCase()), errors };
    }
  },

  getAttributesData(data, tangoDB) {
    const result = {};

    for (const attribute of data.attributes) {
      const {
        device,
        name,
        dataformat,
        datatype,
        unit,
        enumLabels,
        label,
        maxalarm,
        minalarm,
        minvalue,
        maxvalue,
      } = attribute;

      if (name) {
        const fullName = (device + "/" + name).toLowerCase();
        const dataFormat = dataformat.toLowerCase();
        const dataType = datatype;
        const enumlabels = enumLabels;
        const fullPath = createDeviceWithTangoDBFullPath(tangoDB, fullName);
        result[fullPath] = {
          dataFormat,
          dataType,
          unit,
          enumlabels,
          label,
          maxAlarm: maxalarm,
          minAlarm: minalarm,
          minValue: minvalue,
          maxValue: maxvalue,
        };
      }
    }
    return result;
  },

  async fetchAttributeMetadata(tangoDB: string, fullNames: string[]) {
    try {
      fullNames.forEach((fullName) => {
        fullName = fullName.toLowerCase();
      });
      const data = await request(tangoDB, FETCH_ATTRIBUTE_METADATA, {
        fullNames,
      });

      return this.getAttributesData(data, tangoDB);
    } catch (error: any) {
      const errors: string[] = [];
      error.response.errors.map((error) => errors.push(error.desc));
      if (error.response?.data) {// If we still have usefull data on the response we should use it
        return this.getAttributesData(error.response?.data, tangoDB);
      }
      throw errors.join(" ");
    }
  },

  async fetchDeviceState(tangoDB: string, name: string) {
    try {
      const args = { name };
      const data = await request(tangoDB, FETCH_DEVICE_STATE, args);
      return data.device.state;
    } catch (err) {
      return null;
    }
  },

  async fetchAttributesValues(
    tangoDB: string,
    fullNames: string[]
  ): Promise<
    Array<{
      name: string;
      device: string;
      value: any;
      writevalue: any;
      quality: string;
      timestamp: number;
    }>
  > {
    try {
      const data = await request(tangoDB, FETCH_ATTRIBUTES_VALUES, {
        fullNames,
      });
      return data.attributes.map(attribute => ({
        ...attribute,
        name: attribute.name.toLowerCase(),
      }));
    } catch (err) {
      return [];
    }
  },

  async fetchAllClassesAndDevices(tangoDB: string) {
    try {
      const data = await request(tangoDB, FETCH_ALL_CLASSES_WITH_DEVICES);
      return data.classes.map(cls => ({
        ...cls,
        name: cls.name.toLowerCase(),
      }));
    } catch (err) {
      return [];
    }
  },

  async fetchClassAndDevices(tangoDB: string, name: string) {
    try {
      const args = { name };
      const data = await request(tangoDB, FETCH_CLASS_WITH_DEVICES, args);
      return data.classes.map(cls => ({
        ...cls,
        name: cls.name.toLowerCase(),
        devices: cls.devices.map(device => ({
          ...device,
          name: device.name.toLowerCase()
        }))
      }));
    } catch (err) {
      return [];
    }
  },

  async fetchAllClasses(tangoDB: string) {
    try {
      const data = await request(tangoDB, FETCH_ALL_CLASSES);
      return data.classes.map(cls => ({
        ...cls,
        name: cls.name.toLowerCase(),
      }));
    } catch (err) {
      return [];
    }
  },

  async fetchSelectedClassesAndDevices(tangoDB: string, variableNames) {
    if (variableNames?.length === 0) return [];
    let data: any;
    try {
      let query = "{";
      for (const variable of variableNames) {
        query += `
            ${variable.class}: classes(pattern: "${variable.class}") {
              name
              devices {
                name,
                exported,
                connected
              }
            }
          `;
      }
      query += "}";
      data = await request(tangoDB, query);
    } catch (err) {
      return [];
    }

    return data.classes;
  },

  async fetchDevicesProperty(tangoDB: string, variableNames) {
    let data: any;
    try {
      let query = "{";
      for (const variable of variableNames) {
        query += `
            ${sanitize_device_name(variable.device)}: device(name: "${variable.device
          }") {
              name,
              exported,
              connected
            }
          `;
      }
      query += "}";
      data = await request(tangoDB, query);
    } catch (err) {
      return [];
    }

    return Object.values(data);
  },

  async fetchDeviceMetadata(tangoDB: string, deviceNames: string[]) {
    const result = {};

    for (const deviceName of deviceNames) {
      let data: any;
      try {
        data = await request(tangoDB, FETCH_DEVICE_METADATA, { deviceName });
      } catch (err) {
        return null;
      }
      if (data.device) {
        const { alias } = data.device;
        result[deviceName] = { alias };
      }
    }

    return result;
  },
  async fetchDevicesMetadata(tangoDB: string, deviceNames: string[]) {
    let data: any;
    try {
      let query = "{";
      for (const device of deviceNames) {
        query += `
            ${sanitize_device_name(device)}: device(name: "${device}") {
              name, alias
            }
          `;
      }
      query += "}";
      data = await request(tangoDB, query);
    } catch (err) {
      return null;
    }

    return Object.values(data);
  },
};

export default tangoAPIExport;
