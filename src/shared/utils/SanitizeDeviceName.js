// SanitizeDeviceName accept one parameter: device name
// device name is the name of device to sanitize
// return sanitized device name

export default function sanitize_device_name(devicename){
    return devicename.replace(/[^a-zA-Z0-9 ]/g, "_");
}

