import { assert } from 'console';
import { getAllInnerWidgetsById, mergeObjects, calculateInnerWidgetAlignment, calculateInnerWidgetsOrder, hasBoxWidget, isBoxWidgetContained, canBeNested, removeBoxAndInnerWidgets, getParentBoxWidget } from './canvas';

const nextedBoxObj = {
    "1": {
        id: "1",
        type: "BOX",
        innerWidgets: [
            {
                id: "2",
                type: "BOX",
                innerWidgets: [
                    {
                        id: "3",
                        type: "ATTRIBUTE_DISPLAY"
                    }
                ]
            }
        ]
    }
}

const widgetsObject = {
    "1": {
        "_id": "618bea30d62fe100141df74c",
        "id": "1",
        "x": 19,
        "y": 5,
        "canvas": "0",
        "width": 26,
        "height": 21,
        "type": "BOX",
        "inputs": {
            "textColor": "#7bcb3a",
            "backgroundColor": "#ffffff",
            "title": "First Group"
        },
        "innerWidgets": [
            {
                "id": "2",
                "x": 20,
                "y": 7,
                "canvas": "0",
                "width": 10,
                "height": 2,
                "type": "LABEL",
                "inputs": {
                    "text": "HELLO LABEL",
                    "textColor": "#000000"
                },
                "valid": true,
                "order": 10
            },
            {
                "id": "3",
                "x": 21,
                "y": 9,
                "canvas": "0",
                "width": 20,
                "height": 12,
                "type": "SPECTRUM",
                "inputs": {
                    "attribute": {
                        "device": "sys/tg_test/1",
                        "attribute": "double_spectrum_ro",
                        "label": "double_spectrum_ro"
                    }
                },
                "valid": true,
                "order": 11
            }
        ],
        "order": 0,
        "valid": true
    },
    "4": {
        "_id": "618bea30d62fe100141df74d",
        "id": "4",
        "x": 20,
        "y": 3,
        "canvas": "0",
        "width": 10,
        "height": 2,
        "type": "ATTRIBUTE_DISPLAY",
        "inputs": {
            "attribute": {
                "device": "sys/tg_test/1",
                "attribute": "double_scalar",
                "label": "double_scalar"
            }
        },
        "order": 1,
        "valid": true
    }
}

const oldWidgets = {
    "1": {
        "_id": "618bea30d62fe100141df74c",
        "id": "1",
        "x": 17,
        "y": 4,
        "width": 26,
        "height": 18,
        "type": "BOX",
        "inputs": {
            "textColor": "#7bcb3a",
            "fontFamily": "Courier new"
        },
        "innerWidgets": [
            {
                "id": "2",
                "x": 18,
                "y": 6,
                "canvas": "0",
                "width": 10,
                "height": 2,
                "type": "LABEL",
                "inputs": {
                    "text": "HELLO LABEL",
                    "textColor": "#000000",
                    "backgroundColor": "red",
                },
                "valid": true,
                "order": 10
            },
            {
                "id": "3",
                "x": 19,
                "y": 8,
                "canvas": "0",
                "width": 20,
                "height": 12,
                "type": "SPECTRUM",
                "inputs": {
                    "attribute": {
                        "device": "sys/tg_test/1",
                        "attribute": "double_spectrum",
                        "label": "double_spectrum"
                    }
                },
                "valid": true,
                "order": 11
            },
            {
                "id": "5",
                "x": 31,
                "y": 6,
                "canvas": "0",
                "width": 10,
                "height": 2,
                "type": "ATTRIBUTE_DISPLAY",
                "inputs": {
                    "attribute": {
                        "device": "sys/tg_test/1",
                        "attribute": "long_scalar",
                        "label": "long_scalar"
                    },
                    "font": "Helvetica"
                },
                "valid": true,
                "order": 12
            },
            {
                "id": "6",
                "x": 17,
                "y": 4,
                "width": 26,
                "height": 18,
                "type": "BOX",
                "inputs": {
                    "textColor": "#7bcb3a",
                    "fontFamily": "Courier new"
                },
                "innerWidgets": [
                    {
                        "id": "7",
                        "x": 18,
                        "y": 6,
                        "canvas": "0",
                        "width": 10,
                        "height": 2,
                        "type": "LABEL",
                        "inputs": {
                            "text": "HELLO LABEL",
                            "textColor": "#000000",
                            "backgroundColor": "red",
                        },
                        "valid": true,
                        "order": 10
                    }
                ]
            }
        ],
        "order": 0,
        "valid": true
    },
    "4": {
        "_id": "618bea30d62fe100141df74d",
        "id": "4",
        "x": 21,
        "y": 1,
        "canvas": "0",
        "width": 10,
        "height": 2,
        "type": "ATTRIBUTE_DISPLAY",
        "inputs": {
            "attribute": {
                "device": "sys/tg_test/1",
                "attribute": "double_scalar",
                "label": "double_scalar"
            }
        },
        "order": 1,
        "valid": true
    }
}
const replaceWidgets = {
    "5": {
        "id": "5",
        "x": 31,
        "y": 6,
        "canvas": "0",
        "width": 10,
        "height": 2,
        "type": "ATTRIBUTE_DISPLAY",
        "inputs": {
            "attribute": {
                "device": "sys/tg_test/1",
                "attribute": "short_scalar",
                "label": "short_scalar"
            }
        },
        "valid": true,
        "order": 12
    }
}

describe("Test canvas > functions", () =>{
    it("test getAllInnerWidgets", () => {
        let widgets = getAllInnerWidgetsById(widgetsObject);
        //Make sure inner wigets are added to widgets object
        expect(widgets.hasOwnProperty('2')).toBe(true);
        expect(widgets.hasOwnProperty('3')).toBe(true);
        expect(Object.keys(widgets).length).toBe(4);


        widgets = getAllInnerWidgetsById(Object.values(widgetsObject));
        //Make sure inner wigets are added to widgets object
        expect(widgets.hasOwnProperty('2')).toBe(true);
        expect(widgets.hasOwnProperty('3')).toBe(true);
        expect(Object.keys(widgets).length).toBe(4);
    });

    it("test mergeObject", () => {
        const widgets = mergeObjects(oldWidgets, replaceWidgets);
        // Make sure widgets are updated, not added
        expect(widgets.hasOwnProperty('5')).toBe(false);
        expect(JSON.stringify(widgets['1']['innerWidgets'])).toContain('short_scalar');
    });
});

let width = 31
let height = 21
let border = 0
let padding = 0
let TILE_SIZE = 20
let title = "Title"
let alignment = []
let smallWidgetHeight = 15
let bigWidgetHeight = 200
let alignmentLayout = "vertical"

let innerWidgets = [
    {
        "id": "6",
        "x": 44,
        "y": 16,
        "canvas": "0",
        "width": 28,
        "height": smallWidgetHeight,
        "type": "ATTRIBUTE WRITER DROPDOWN",
        "inputs": {
            "attribute": {
                "device": "sys/tg_test/1",
                "attribute": "boolean_scalar",
                "label": "boolean_scalar"
            },
            "writeValues": [],
            "showDevice": true,
            "showAttribute": "Label",
            "textColor": "#000000",
            "backgroundColor": "#ffffff",
            "size": 1,
            "font": "Helvetica"
        },
        "order": 2
    },
    {
        "id": "5",
        "x": 48,
        "y": 15,
        "canvas": "0",
        "width": 30,
        "height": bigWidgetHeight,
        "type": "ATTRIBUTE_PLOT",
        "inputs": {
            "timeWindow": 120,
            "showZeroLine": true,
            "logarithmic": false,
            "attributes": [
                {
                    "attribute": {
                        "device": "sys/tg_test/1",
                        "attribute": "short_scalar_ro",
                        "label": "short_scalar_ro"
                    },
                    "showAttribute": "Label",
                    "yAxis": "left"
                },
                {
                    "attribute": {
                        "device": "sys/tg_test/1",
                        "attribute": "double_scalar",
                        "label": "double_scalar"
                    },
                    "showAttribute": "Label",
                    "yAxis": "left"
                }
            ]
        },
        "order": 20
    }
]

let boxWidget = {
    "_id": "1",
    "id": "1",
    "x": 22,
    "y": 15,
    "canvas": "0",
    "width": width,
    "height": height,
    "type": "BOX",
    "inputs": {
        "textColor": "#000000",
        "backgroundColor": "#fcfcfc",
        "title": title,
        "borderColor": "#e942ff",
        "borderWidth": border,
        "textSize": 1,
        "layout": alignmentLayout,
        "padding": padding,
        "customCss": "",
        "borderStyle": "dashed",
        "fontFamily": "Courier new",
        "bigWidget": 7,
        "smallWidget": 1
    },
    "innerWidgets": innerWidgets,
    "order": 0,
    "valid": true
}

let legacyWidget = {
    "_id": "1",
    "id": "1",
    "x": 22,
    "y": 15,
    "canvas": "0",
    "width": width,
    "height": height,
    "type": "BOX",
    "inputs": {
        "textColor": "#000000",
        "backgroundColor": "#fcfcfc",
        "title": "",
        "borderColor": "#e942ff",
        "borderWidth": border,
        "textSize": 1,
        "layout": alignmentLayout,
        "padding": padding,
        "customCss": "",
        "borderStyle": "dashed",
        "fontFamily": "Courier new",
        "bigWidget": 7,
        "smallWidget": 1
    },
    "order": 0,
    "valid": true
}

describe("Test Canvas util functions", () =>{
    it("test if alignment function creates the alignment object with no element ", ()=>{
        alignment = calculateInnerWidgetAlignment(legacyWidget, TILE_SIZE); 
        expect(alignment.length).toBe(0)
    })

    

    it("test if alignment function creates the alignment object with two element ", ()=>{
        alignment = calculateInnerWidgetAlignment(boxWidget, TILE_SIZE); 
        expect(alignment.length).toBe(2)
        
    })

    it("verify if alignment function calculates properly width", ()=>{
        alignment = calculateInnerWidgetAlignment(boxWidget, TILE_SIZE); 
        expect(alignment[0].width).toBe(width * TILE_SIZE)
        
    })

    it("verify if alignment function arranges width based on border", ()=>{
        alignment = calculateInnerWidgetAlignment(boxWidget, TILE_SIZE); 

        border = 2
        boxWidget = {
            "_id": "1",
            "id": "1",
            "x": 22,
            "y": 15,
            "canvas": "0",
            "width": width,
            "height": height,
            "type": "BOX",
            "inputs": {
                "textColor": "#000000",
                "backgroundColor": "#fcfcfc",
                "title": title,
                "borderColor": "#e942ff",
                "borderWidth": border,
                "textSize": 1,
                "layout": alignmentLayout,
                "padding": padding,
                "customCss": "",
                "borderStyle": "dashed",
                "fontFamily": "Courier new",
                "bigWidget": 7,
                "smallWidget": 1
            },
            "innerWidgets": innerWidgets,
            "order": 0,
            "valid": true
        }
        alignment = calculateInnerWidgetAlignment(boxWidget, TILE_SIZE); 
        expect(alignment[0].width).toBe((width * TILE_SIZE) - (2*border))
        
    })

    it("verify if alignment function calculated height different from widget height", ()=>{

        smallWidgetHeight = 1
        bigWidgetHeight = 1

        title = ""
        
        innerWidgets = [
            {
                "id": "6",
                "x": 44,
                "y": 16,
                "canvas": "0",
                "width": 28,
                "height": bigWidgetHeight,
                "type": "ATTRIBUTE WRITER DROPDOWN",
                "inputs": {
                    "attribute": {
                        "device": "sys/tg_test/1",
                        "attribute": "boolean_scalar",
                        "label": "boolean_scalar"
                    },
                    "writeValues": [],
                    "showDevice": true,
                    "showAttribute": "Label",
                    "textColor": "#000000",
                    "backgroundColor": "#ffffff",
                    "size": 1,
                    "font": "Helvetica"
                },
                "order": 2
            },
            {
                "id": "5",
                "x": 48,
                "y": 15,
                "canvas": "0",
                "width": 30,
                "height": bigWidgetHeight,
                "type": "ATTRIBUTE_PLOT",
                "inputs": {
                    "timeWindow": 120,
                    "showZeroLine": true,
                    "logarithmic": false,
                    "attributes": [
                        {
                            "attribute": {
                                "device": "sys/tg_test/1",
                                "attribute": "short_scalar_ro",
                                "label": "short_scalar_ro"
                            },
                            "showAttribute": "Label",
                            "yAxis": "left"
                        },
                        {
                            "attribute": {
                                "device": "sys/tg_test/1",
                                "attribute": "double_scalar",
                                "label": "double_scalar"
                            },
                            "showAttribute": "Label",
                            "yAxis": "left"
                        }
                    ]
                },
                "order": 20
            }
        ]

        boxWidget = {
            "_id": "1",
            "id": "1",
            "x": 22,
            "y": 15,
            "canvas": "0",
            "width": width,
            "height": height,
            "type": "BOX",
            "inputs": {
                "textColor": "#000000",
                "backgroundColor": "#fcfcfc",
                "title": title,
                "borderColor": "#e942ff",
                "borderWidth": border,
                "textSize": 1,
                "layout": alignmentLayout,
                "padding": padding,
                "customCss": "",
                "borderStyle": "dashed",
                "fontFamily": "Courier new",
                "bigWidget": 7,
                "smallWidget": 1
            },
            "innerWidgets": innerWidgets,
            "order": 0,
            "valid": true
        }

        let alignmentSmall = calculateInnerWidgetAlignment(boxWidget, TILE_SIZE); 
        expect(alignmentSmall[0].height).not.toBe(alignment[0].height)
        expect(alignmentSmall[1].height).not.toBe(alignment[1].height)
        
    })

    it("verify if alignment function works also with the horizontal alignment", ()=>{

        smallWidgetHeight = 1
        bigWidgetHeight = 1
        
        innerWidgets = [
            {
                "id": "6",
                "x": 44,
                "y": 16,
                "canvas": "0",
                "width": 28,
                "height": bigWidgetHeight,
                "type": "ATTRIBUTE WRITER DROPDOWN",
                "inputs": {
                    "attribute": {
                        "device": "sys/tg_test/1",
                        "attribute": "boolean_scalar",
                        "label": "boolean_scalar"
                    },
                    "writeValues": [],
                    "showDevice": true,
                    "showAttribute": "Label",
                    "textColor": "#000000",
                    "backgroundColor": "#ffffff",
                    "size": 1,
                    "font": "Helvetica"
                },
                "order": 2
            },
            {
                "id": "5",
                "x": 48,
                "y": 15,
                "canvas": "0",
                "width": 30,
                "height": bigWidgetHeight,
                "type": "ATTRIBUTE_PLOT",
                "inputs": {
                    "timeWindow": 120,
                    "showZeroLine": true,
                    "logarithmic": false,
                    "attributes": [
                        {
                            "attribute": {
                                "device": "sys/tg_test/1",
                                "attribute": "short_scalar_ro",
                                "label": "short_scalar_ro"
                            },
                            "showAttribute": "Label",
                            "yAxis": "left"
                        },
                        {
                            "attribute": {
                                "device": "sys/tg_test/1",
                                "attribute": "double_scalar",
                                "label": "double_scalar"
                            },
                            "showAttribute": "Label",
                            "yAxis": "left"
                        }
                    ]
                },
                "order": 20
            }
        ]

        alignmentLayout = "horizontal"

        boxWidget = {
            "_id": "1",
            "id": "1",
            "x": 22,
            "y": 15,
            "canvas": "0",
            "width": width,
            "height": height,
            "type": "BOX",
            "inputs": {
                "textColor": "#000000",
                "backgroundColor": "#fcfcfc",
                "title": title,
                "borderColor": "#e942ff",
                "borderWidth": border,
                "textSize": 1,
                "layout": alignmentLayout,
                "padding": padding,
                "customCss": "",
                "borderStyle": "dashed",
                "fontFamily": "Courier new",
                "bigWidget": 7,
                "smallWidget": 1
            },
            "innerWidgets": innerWidgets,
            "order": 0,
            "valid": true
        }

        let alignment = calculateInnerWidgetAlignment(boxWidget, TILE_SIZE); 
        expect(alignment[0].height).toBe(alignment[1].height);         
    })

    it("verify if alignment function changes the width calculated different from widget size", ()=>{

        smallWidgetHeight = 1
        bigWidgetHeight = 1
        
        innerWidgets = [
            {
                "id": "6",
                "x": 44,
                "y": 16,
                "canvas": "0",
                "width": 28,
                "height": bigWidgetHeight,
                "type": "ATTRIBUTE WRITER DROPDOWN",
                "inputs": {
                    "attribute": {
                        "device": "sys/tg_test/1",
                        "attribute": "boolean_scalar",
                        "label": "boolean_scalar"
                    },
                    "writeValues": [],
                    "showDevice": true,
                    "showAttribute": "Label",
                    "textColor": "#000000",
                    "backgroundColor": "#ffffff",
                    "size": 1,
                    "font": "Helvetica"
                },
                "order": 2
            },
            {
                "id": "5",
                "x": 48,
                "y": 15,
                "canvas": "0",
                "width": 30,
                "height": bigWidgetHeight,
                "type": "ATTRIBUTE_PLOT",
                "inputs": {
                    "timeWindow": 120,
                    "showZeroLine": true,
                    "logarithmic": false,
                    "attributes": [
                        {
                            "attribute": {
                                "device": "sys/tg_test/1",
                                "attribute": "short_scalar_ro",
                                "label": "short_scalar_ro"
                            },
                            "showAttribute": "Label",
                            "yAxis": "left"
                        },
                        {
                            "attribute": {
                                "device": "sys/tg_test/1",
                                "attribute": "double_scalar",
                                "label": "double_scalar"
                            },
                            "showAttribute": "Label",
                            "yAxis": "left"
                        }
                    ]
                },
                "order": 20
            }
        ]

        alignmentLayout = "horizontal"

        boxWidget = {
            "_id": "1",
            "id": "1",
            "x": 22,
            "y": 15,
            "canvas": "0",
            "width": width,
            "height": height,
            "type": "BOX",
            "inputs": {
                "textColor": "#000000",
                "backgroundColor": "#fcfcfc",
                "title": title,
                "borderColor": "#e942ff",
                "borderWidth": border,
                "textSize": 1,
                "layout": alignmentLayout,
                "padding": padding,
                "customCss": "",
                "borderStyle": "dashed",
                "fontFamily": "Courier new",
                "bigWidget": 7,
                "smallWidget": 1
            },
            "innerWidgets": innerWidgets,
            "order": 0,
            "valid": true
        }

        let alignment = calculateInnerWidgetAlignment(boxWidget, TILE_SIZE); 
        expect(alignment[0].height).toBe(alignment[1].height);
    })

    it("test calculateInnerWidgetsOrder function", () => {
        let parentWidget = {
            "id": "1",
            "x": 28,
            "y": 6,
            "canvas": "0",
            "width": 20,
            "height": 11,
            "type": "BOX",
            "inputs": {
                "title": "",
                "bigWidget": 10,
                "smallWidget": 1,
                "textColor": "#000000",
                "backgroundColor": "#ffffff",
                "borderColor": "",
                "borderWidth": 0,
                "borderStyle": "none",
                "textSize": 1,
                "fontFamily": "Helvetica",
                "layout": "vertical",
                "padding": 0,
                "customCss": ""
            },
            "valid": true,
            "order": 0,
            "innerWidgets": [
                {
                    "id": "2",
                    "x": 28.4,
                    "y": 8.280000000000001,
                    "canvas": "0",
                    "width": 19.2,
                    "height": 3.2,
                    "type": "LABEL",
                    "inputs": {
                        "text": "1",
                        "textColor": "#000000",
                        "backgroundColor": "#ffffff",
                        "size": 1,
                        "borderWidth": 0,
                        "borderColor": "#000000",
                        "font": "Helvetica",
                        "linkTo": "",
                        "customCss": ""
                    },
                    "valid": true,
                    "order": 1
                },
                {
                    "id": "4",
                    "x": 34,
                    "y": 12,
                    "canvas": "0",
                    "width": 10,
                    "height": 2,
                    "type": "LABEL",
                    "inputs": {
                        "text": "",
                        "textColor": "#000000",
                        "backgroundColor": "#ffffff",
                        "size": 1,
                        "borderWidth": 0,
                        "borderColor": "#000000",
                        "font": "Helvetica",
                        "linkTo": "",
                        "customCss": ""
                    },
                    "valid": true,
                    "order": 2
                },
                {
                    "id": "3",
                    "x": 28.4,
                    "y": 11.48,
                    "canvas": "0",
                    "width": 19.2,
                    "height": 3.2,
                    "type": "LABEL",
                    "inputs": {
                        "text": "2",
                        "textColor": "#000000",
                        "backgroundColor": "#ffffff",
                        "size": 1,
                        "borderWidth": 0,
                        "borderColor": "#000000",
                        "font": "Helvetica",
                        "linkTo": "",
                        "customCss": ""
                    },
                    "valid": true,
                    "order": 3
                }
            ]
        };

        let newWidget = {
            "id": "4",
            "x": 34,
            "y": 12,
            "canvas": "0",
            "width": 10,
            "height": 2,
            "type": "LABEL",
            "inputs": {
                "text": "",
                "textColor": "#000000",
                "backgroundColor": "#ffffff",
                "size": 1,
                "borderWidth": 0,
                "borderColor": "#000000",
                "font": "Helvetica",
                "linkTo": "",
                "customCss": ""
            },
            "valid": true,
            "order": 2
        }; 

        let firstElementBeforeMoving = parentWidget.innerWidgets[0].order
        let lastElementBeforeMoving = parentWidget.innerWidgets[1].order
        calculateInnerWidgetsOrder(parentWidget, newWidget, 34, 12, 20); 
        assert(parentWidget.innerWidgets[0].order = firstElementBeforeMoving);
        assert(parentWidget.innerWidgets[1].order > lastElementBeforeMoving);

        parentWidget = {
            "_id": "61a0ea5ad708c90013a789df",
            "id": "1",
            "x": 27,
            "y": 8,
            "canvas": "0",
            "width": 20,
            "height": 11,
            "type": "BOX",
            "inputs": {
                "title": "",
                "bigWidget": 10,
                "smallWidget": 1,
                "textColor": "#000000",
                "backgroundColor": "#ffffff",
                "borderColor": "",
                "borderWidth": 0,
                "borderStyle": "none",
                "textSize": 1,
                "fontFamily": "Helvetica",
                "layout": "horizontal",
                "padding": 0,
                "customCss": ""
            },
            "order": 0,
            "innerWidgets": [
                {
                    "id": "2",
                    "x": 27.4,
                    "y": 10.28,
                    "canvas": "0",
                    "width": 6.4,
                    "height": 9.72,
                    "type": "LABEL",
                    "inputs": {
                        "text": "1",
                        "textColor": "#000000",
                        "backgroundColor": "#ffffff",
                        "size": 1,
                        "borderWidth": 0,
                        "borderColor": "#000000",
                        "font": "Helvetica",
                        "linkTo": "",
                        "customCss": ""
                    },
                    "valid": true,
                    "order": 1
                },
                {
                    "id": "4",
                    "x": 33.8,
                    "y": 10.28,
                    "canvas": "0",
                    "width": 6.4,
                    "height": 9.72,
                    "type": "LABEL",
                    "inputs": {
                        "text": "",
                        "textColor": "#000000",
                        "backgroundColor": "#ffffff",
                        "size": 1,
                        "borderWidth": 0,
                        "borderColor": "#000000",
                        "font": "Helvetica",
                        "linkTo": "",
                        "customCss": ""
                    },
                    "valid": true,
                    "order": 3
                },
                {
                    "id": "3",
                    "x": 40.2,
                    "y": 10.28,
                    "canvas": "0",
                    "width": 6.4,
                    "height": 9.72,
                    "type": "LABEL",
                    "inputs": {
                        "text": "2",
                        "textColor": "#000000",
                        "backgroundColor": "#ffffff",
                        "size": 1,
                        "borderWidth": 0,
                        "borderColor": "#000000",
                        "font": "Helvetica",
                        "linkTo": "",
                        "customCss": ""
                    },
                    "valid": true,
                    "order": 4
                }
            ],
            "valid": true
        }

        firstElementBeforeMoving = parentWidget.innerWidgets[0].order
        lastElementBeforeMoving = parentWidget.innerWidgets[1].order
        calculateInnerWidgetsOrder(parentWidget, newWidget, 27, 12, 20); 
        assert(parentWidget.innerWidgets[0].order = firstElementBeforeMoving);
        assert(parentWidget.innerWidgets[1].order > lastElementBeforeMoving);
    })

    it("test hasBoxWidget function", () => {

        expect(hasBoxWidget(widgetsObject)).toBe(true);
        expect(hasBoxWidget(delete widgetsObject['4'])).toBe(false);
    });

    it("test hasBoxWidget function", () => {

        expect(isBoxWidgetContained(oldWidgets['1'], '2', true)).toBe(true);
        expect(isBoxWidgetContained(oldWidgets['1'], '6', true)).toBe(true);
        expect(isBoxWidgetContained(oldWidgets['1'], '4', true)).toBe(false);
    });

    it("test canBeNested function", () => {

        let innerWidgets = [
            {
                "id": "6",
                "x": 44,
                "y": 16,
                "canvas": "0",
                "width": 28,
                "height": smallWidgetHeight,
                "type": "BOX",
                "inputs": {
                    "attribute": {
                        "device": "sys/tg_test/1",
                        "attribute": "boolean_scalar",
                        "label": "boolean_scalar"
                    },
                    "writeValues": [],
                    "showDevice": true,
                    "showAttribute": "Label",
                    "textColor": "#000000",
                    "backgroundColor": "#ffffff",
                    "size": 1,
                    "font": "Helvetica"
                },
                "order": 2
            },
            {
                "id": "5",
                "x": 48,
                "y": 15,
                "canvas": "0",
                "width": 30,
                "height": bigWidgetHeight,
                "type": "ATTRIBUTE_PLOT",
                "inputs": {
                    "timeWindow": 120,
                    "showZeroLine": true,
                    "logarithmic": false,
                    "attributes": [
                        {
                            "attribute": {
                                "device": "sys/tg_test/1",
                                "attribute": "short_scalar_ro",
                                "label": "short_scalar_ro"
                            },
                            "showAttribute": "Label",
                            "yAxis": "left"
                        },
                        {
                            "attribute": {
                                "device": "sys/tg_test/1",
                                "attribute": "double_scalar",
                                "label": "double_scalar"
                            },
                            "showAttribute": "Label",
                            "yAxis": "left"
                        }
                    ]
                },
                "order": 20
            }
        ]
        
        let boxWidget = {
            "_id": "1",
            "id": "1",
            "x": 22,
            "y": 15,
            "canvas": "0",
            "width": width,
            "height": height,
            "type": "BOX",
            "inputs": {
                "textColor": "#000000",
                "backgroundColor": "#fcfcfc",
                "title": title,
                "borderColor": "#e942ff",
                "borderWidth": border,
                "textSize": 1,
                "layout": alignmentLayout,
                "padding": padding,
                "customCss": "",
                "borderStyle": "dashed",
                "fontFamily": "Courier new",
                "bigWidget": 7,
                "smallWidget": 1
            },
            "innerWidgets": innerWidgets,
            "order": 0,
            "valid": true
        }

        let BoxWidgetCanvas = [boxWidget, boxWidget]

        expect(canBeNested(boxWidget, '6', BoxWidgetCanvas)).toBe(false); 
    })

    it("test removeBoxAndInnerWidgets function", () => {

        let selectedWidgetIds = ["1","2","3","4"];

        removeBoxAndInnerWidgets(Object.values(widgetsObject), selectedWidgetIds);
        expect(selectedWidgetIds).toEqual(["4"]);

        selectedWidgetIds = ["4"]
        removeBoxAndInnerWidgets(Object.values(widgetsObject), ["4"])
        expect(selectedWidgetIds).toEqual(["4"]);
    });

    it("test getParentBoxWidget function", () => {
        expect(getParentBoxWidget([])).toEqual(null);
        expect(getParentBoxWidget([], "2")).toEqual(null);
        expect(getParentBoxWidget(nextedBoxObj, "2").id).toEqual("1");
        expect(getParentBoxWidget(nextedBoxObj, "3").id).toEqual("2");
    });
})
