import React, { useState } from "react";
import { getSubs } from "../../utils/getSubs";
import { getDeviceFromPath } from "../../../dashboard/runtime/utils";
import MetricsModal from "./MetricsModal";

import { IRootState } from "../../state/reducers/rootReducer";
import { useSelector } from "react-redux";
import { SubscribedAttr } from "../../state/types/types";

interface Props {
    selectedTab: string;
    metricsDataFetched: boolean;
}

/**
 * This returns metrics LED
 *
 * @param props
 * @return
 */
const GetMetricLed: React.FC<Props> = (props) => {
    const [showModal, setShowModal] = useState(false);
    const widgets = useSelector((state: IRootState) => {
        return state.selectedDashboard.widgets;
    });
    const metricsError: string = useSelector((state: IRootState) => {
        return state.communicationHealth?.error;
    });
    const subscribedAttrs: SubscribedAttr[] = useSelector((state: IRootState) => {
        return state.communicationHealth?.metrics?.subscribedAttrs;
    });
    const attributes = useSelector((state: IRootState) => {
        return state.attributes;
    });

    let LedColor = "bg-success"
    let titleMsg = "The dashboard is receiving values properly!"

    if (metricsError) {
        LedColor = "error-triangle"
        titleMsg = "Taranta is not receiving metrics data. Click to learn more."
    } else if ("devices" === props.selectedTab) {
        const unavailableAttr = subscribedAttrs?.filter(r => (r.listeners > 0 && !r.deviceAccessible));

        if (Object.keys(attributes).length === 0) {
            LedColor = "bg-secondary"
            titleMsg = "Taranta has not subscribed to any attribute."

        } else if (unavailableAttr) {
            if (unavailableAttr.length > 0) {
                LedColor = "bg-warning rounded-0"
                titleMsg = "The dashboard is not receiving values for some of the components! Click to learn more."
            }
        }

    } else if (Object.keys(widgets).length > 0) {
        const subs = getSubs(widgets) || [];
        const dashboardAttrs = subs.reduce((acc: string[], r) => {
            acc?.push(getDeviceFromPath(r))
            return acc;
        }, [])

        const accessibleAttrList = subscribedAttrs?.reduce((acc: string[], r) => {
            if (r.listeners > 0 && r.deviceAccessible)
                acc.push(r.name)
            return acc
        }, [])

        const diff = dashboardAttrs.filter((attr) => !accessibleAttrList?.includes(attr));
        if (!props.metricsDataFetched || 0 === dashboardAttrs.length) {
            //Either metrics details not fetched yet or dashboard does not have subscription
            LedColor = "bg-secondary"
            titleMsg = "Taranta has not subscribed to any attribute."

        } else if (diff.length && diff.length === dashboardAttrs.length) {
            LedColor = "error-triangle border-yellow"
            titleMsg = "The dashboard is not receiving values for any of the component! Click to learn more."

        } else if (diff.length && diff.length < dashboardAttrs?.length) {
            LedColor = "bg-warning rounded-0"
            titleMsg = "The dashboard is not receiving values for some of the components! Click to learn more."
        }
    }

    return (
        <div>
            <div
                className="metrics-led-wrapper"
                title={titleMsg}
                onClick={() => setShowModal(true)}
            >
                <div className={"metrics-led " + LedColor}></div>
            </div>

            {showModal && (
                <MetricsModal setState={({ showModal }) => setShowModal(showModal)} />
            )}
        </div>
    );
};

export default GetMetricLed;
