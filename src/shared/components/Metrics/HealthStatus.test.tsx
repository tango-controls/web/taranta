import React from 'react';
import { render } from '@testing-library/react';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import GetMetricLedStatus from './HealthStatus';

// Create a mock Redux store
const mockStore = configureStore([]);

describe('HealthStatus GetMetricLEDStatus', () => {
  it('Dashboard with Green LED', () => {
    // Define the mock state
    const mockState = {
        selectedDashboard: {
            widgets:{}
        },
        communicationHealth: {
            metrics: {
                subscribedAttrs: []
            }
        }
    };

    // Create a mock store with the desired state
    const store = mockStore(mockState);

    // Render the component within the Provider and the mock store
    const {container} = render(
      <Provider store={store}>
        <GetMetricLedStatus selectedTab='Dashboard' metricsDataFetched={true} />
      </Provider>
    );

    // Assert and expect statements
    expect(container.innerHTML).toContain('metrics-led-wrapper');
    expect(container.innerHTML).toContain('bg-success');
  });

  it('Dashboard with Orange LED', () => {
    // Define the mock state
    const mockState = {
      selectedDashboard: {
          widgets:{
            '2': {
            valid: 1,
            _id: '663ce5c81117e20012707d59',
            id: '2',
            x: 17,
            y: 25,
            type: 'ATTRIBUTE_DISPLAY',
            inputs: {
              attribute: {
                device: 'testdb://sys/tg_test/1',
                attribute: 'short_scalar',
                label: 'short_scalar'
              },
            },
            order: 1,
            disabled: false
          },
          '3': {
            valid: 1,
            _id: '663ce61f1117e20012707d64',
            id: '3',
            x: 17,
            y: 28,
            width: 13,
            height: 2,
            type: 'ATTRIBUTE_DISPLAY',
            inputs: {
              attribute: {
                device: 'testdb://sys/tg_test/1',
                attribute: 'long_scalar',
                label: 'long_scalar'
              },
            },
            order: 2,
            disabled: false
          }
        },
      },
      communicationHealth: {
        metrics: {
          subscribedAttrs: [
            {
              name: 'sys/tg_test/1/short_scalar',
              attribute: 'short_scalar',
              listeners: 1,
              eventType: 'POLLING',
              deviceAccessible: false
            },
            {
              name: 'sys/tg_test/1/long_scalar',
              attribute: 'long_scalar',
              listeners: 1,
              eventType: 'POLLING',
              deviceAccessible: true
            },
          ]
        }
      }
    };

    // Metrics LED with gray color i.e metrics data call is in progress
    const store = mockStore(mockState);
    // Render the component within the Provider and the mock store
    const result = render(
      <Provider store={store}>
        <GetMetricLedStatus selectedTab='Dashboard' metricsDataFetched={false}/>
      </Provider>
    );
    expect(result.container.innerHTML).toContain('bg-secondary');

    //Metrics LED with orange color, metrics data recieved with some attrs not accessible
    const {container} = render(
      <Provider store={store}>
        <GetMetricLedStatus selectedTab='Dashboard' metricsDataFetched={true}/>
      </Provider>
    );

    expect(container.innerHTML).toContain('bg-warning');


    mockState.communicationHealth.metrics.subscribedAttrs[1].deviceAccessible = false
    const store1 = mockStore(mockState);
    // Render the component within the Provider and the mock store
    const res = render(
      <Provider store={store1}>
        <GetMetricLedStatus selectedTab='Dashboard' metricsDataFetched={true}/>
      </Provider>
    );
    expect(res.container.innerHTML).toContain('error-triangle');
    expect(res.container.innerHTML).toContain('The dashboard is not receiving values for any of the component! Click to learn more.')
  });

  it('Devices tab with Green & Orange LED', () => {
    // Define the mock state
    const mockState = {
      selectedDashboard: {
      },
      attributes: {
        'testdb://sys/tg_test/1': {
          state: {
            name: 'state',
            label: 'State',
          }
        }
      },
      communicationHealth: {
        metrics: {
          subscribedAttrs: [
            {
              name: 'sys/tg_test/1/short_scalar',
              attribute: 'short_scalar',
              listeners: 1,
              eventType: 'POLLING',
              deviceAccessible: true
            },
            {
              name: 'sys/tg_test/1/long_scalar',
              attribute: 'long_scalar',
              listeners: 1,
              eventType: 'POLLING',
              deviceAccessible: true
            },
          ]
        }
      }
    };

    // Create a mock store with the desired state
    const store = mockStore(mockState);
    // Render the component within the Provider and the mock store
    const {container} = render(
      <Provider store={store}>
        <GetMetricLedStatus selectedTab='devices' metricsDataFetched={true}/>
      </Provider>
    );

    expect(container.innerHTML).toContain('bg-success');

    mockState.communicationHealth.metrics.subscribedAttrs[1].deviceAccessible = false
    const store1 = mockStore(mockState);
    // Render the component within the Provider and the mock store
    const res = render(
      <Provider store={store1}>
        <GetMetricLedStatus selectedTab='devices' metricsDataFetched={true}/>
      </Provider>
    );
    expect(res.container.innerHTML).toContain('bg-warning rounded-0');
    expect(res.container.innerHTML).toContain('The dashboard is not receiving values for some of the components! Click to learn more')
  });

  it('Devices tab gray LED', () => {
    // Define the mock state
    const mockState = {
      selectedDashboard: {
      },
      attributes: {},
      communicationHealth: {
        metrics: {
          subscribedAttrs: [
          ]
        }
      }
    };

    // Create a mock store with the desired state
    const store = mockStore(mockState);
    // Render the component within the Provider and the mock store
    const {container} = render(
      <Provider store={store}>
        <GetMetricLedStatus selectedTab='devices' metricsDataFetched={true}/>
      </Provider>
    );

    // User is on devices tab with no attributes subscription hence LED color would be gray 
    expect(container.innerHTML).toContain('bg-secondary');
  });
});
