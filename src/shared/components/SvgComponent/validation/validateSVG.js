const layerTag = "layer";
const cssTag = "css";

export function validateSVG(svgContent, elements, attributeList) {
    const errors = [];

    if (elements?.rules?.length > 0) {
        const invalidRulesErrors = validateRules(elements);
        const svgString = new XMLSerializer().serializeToString(svgContent);
        const layerErrors = validateLayers(elements, svgString);
        const cssErrors = validateCSS(elements);
        const attributesErrors = validateAttributes(elements, attributeList);

        errors.push(...invalidRulesErrors, ...layerErrors, ...cssErrors, ...attributesErrors);
    }

    const linkErrors = validateLinks(svgContent);
    errors.push(...linkErrors);

    return {
        isValid: errors.length === 0,
        errors
    };
}

export function validateAttributes(elements, attributeList) {
    const errors = [];
    if(elements.rules !== undefined) {
        const validModels = attributeList?.map(device => `${device.deviceName}/${device.name}`);
        elements.rules.forEach(item => {
            if(item.model !== undefined && (!validModels.includes(item.model))) {
                errors.push({
                    item,
                    error: `Attribute '${item.model}' not present.`
                });
            }
        });
    }
    return errors;
}


function validateRules(elements) {
    const errors = [];
    let rules = elements.rules?.filter(item => item.type !== layerTag && item.type !== cssTag) ?? [];

    rules.forEach((rule) => {
        errors.push({error: `Rule '${rule.type}' not valid.`});
    });
    return errors;
}

export function validateLayers(elements, svgString) {
    
    const errors = [];

    // Allowed operators and character checks
    const allowedOperators = [">", "<", ">=", "<=", "==", "!=", "&&", "||"];
    const allowedCharsRegex = /^[\s0-9a-zA-Z()><=!&|]*$/;

    // Normalize SVG content by removing newlines
    const normalizedsvgString = svgString.replace(/\r?\n|\r/g, ' ');

    // Filter rules of type layerTag
    const layers = elements.rules?.filter(item => item.type === layerTag) ?? [];

    layers.forEach(rule => {
        const { layer, condition } = rule;

        // 1. Check if layer exists in the SVG by substring search
        const expectedLabel = `inkscape:label="${layer}"`;
        if (!normalizedsvgString.includes(expectedLabel)) {
            errors.push({ rule, error: `Layer "${layer}" not found in SVG. Expected attribute: ${expectedLabel}` });
            return;
        }

        // 2. Validate the condition
        if (!condition) {
            errors.push({ rule, error: `Condition is empty or missing.` });
            return;
        }

        if (!allowedCharsRegex.test(condition)) {
            errors.push({ rule, error: `Condition "${condition}" contains invalid characters.` });
            return;
        }

        if (!allowedOperators.some(op => condition.includes(op))) {
            errors.push({ rule, error: `Condition "${condition}" does not contain a valid operator.` });
            return;
        }

        if (!/value/.test(condition)) {
            errors.push({ rule, error: `Condition "${condition}" does not reference "value".` });
            return;
        }

        // Validate condition evaluation
        const conditionError = validateCondition(condition);
        if (conditionError) {
            errors.push({ rule, error: conditionError });
        }
    });

    return errors;
}

// Helper function to validate condition logic
function validateCondition(condition) {
    if (condition.includes('and') || condition.includes('or')) return null; // Skip for chained conditions

    const numericMatches = condition.match(/-?\d+(?:\.\d+)?/g) || [];
    const numericValues = numericMatches.map(num => parseFloat(num));

    const baseTestValues = [-100, 0, 50, 100, 1000];
    const testValues = Array.from(new Set([...baseTestValues, ...numericValues]));

    let alwaysTrue = true;
    let alwaysFalse = true;

    try {
        for (const val of testValues) {
            const testCondition = condition.replace(/value/g, val.toString());
            const result = new Function("return (" + testCondition + ")")();

            if (typeof result !== "boolean") {
                return `Condition "${condition}" did not evaluate to a boolean.`;
            }

            if (result) alwaysFalse = false;
            else alwaysTrue = false;
        }

        if (alwaysTrue) return `Condition "${condition}" is always true.`;
        if (alwaysFalse) return `Condition "${condition}" is always false.`;
    } catch (e) {
        return `Condition "${condition}" is invalid or could not be evaluated: ${e.message}`;
    }

    return null;
}


export function validateLinks(svgContent) {
    const errors = [];
    const urlRegex = /^(https?|ftp):\/\/[^\s/$.?#].[^\s]*$/i;

    svgContent.querySelectorAll('a').forEach(r => {
        const anchorLink = r.getAttribute('xlink:href');

        if (anchorLink && !urlRegex.test(anchorLink)) {
            const idText = r.getAttribute("id") ? (`with Id "${r.getAttribute("id")}"`) : "";
            errors.push({
                error: `Anchor ${idText} has invalid link "${anchorLink}".`
            })
        }
    })

    return errors;
}

function validateCSS(elements) {
    const errors = [];
    // let css = elements.rules?.filter(item => item.type === "css")

    return errors;
}

export default validateSVG
