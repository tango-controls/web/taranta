/* eslint-disable jsx-a11y/anchor-is-valid */

import React, { Fragment } from "react";
import { connect } from "react-redux";

import {
  logout,
  openLoginDialog,
} from "../../../../shared/user/state/actionCreators";

import {
  getIsLoggedIn,
  getUsername,
  getAwaitingResponse,
  getLoginFailure,
} from "../../../../shared/user/state/selectors";
import "./LogInOut.css";

import preval from "preval.macro";
import { getTangoHost } from "../../../state/selectors/database";

const buildDate =
  `Build date: ` + preval`module.exports = new Date().toUTCString();`;

const WhenLoggedIn = ({ username, onPressLogout, version, tangoHost }) => (
  <Fragment>
    <a
      className="versionCSS"
      title={`Version: ${version}\n${buildDate}\nTANGO_HOST: ${tangoHost}`}
      href="https://taranta.readthedocs.io/en/latest/history.html"
      target="_blank"
      rel="noreferrer"
    >
      {version}
    </a>
    <span style={{ fontWeight: "bold" }}>{username}</span>{" "}
    <a
      href="#"
      onClick={(e) => {
        e.preventDefault();
        onPressLogout();
      }}
    >
      Log Out
    </a>
  </Fragment>
);

const WhenLoggedOut = ({ onPressLogin, version, tangoHost }) => (
  <Fragment>
    <a
      className="versionCSS"
      title={`Version: ${version}\n${buildDate}\nTANGO_HOST: ${tangoHost}`}
    >
      {version}{" "}
    </a>
    Not logged in.{" "}
    <a
      href="#"
      onClick={(e) => {
        e.preventDefault();
        onPressLogin();
      }}
    >
      Log In
    </a>
  </Fragment>
);

interface State {
  showingModal: boolean;
  username: string;
  password: string;
}

type Props = StateProps & DispatchProps;

class LogInOut extends React.Component<Props, State> {
  constructor(props) {
    super(props);
    this.state = {
      showingModal: false,
      username: "",
      password: "",
    };
  }

  public render() {
    const {
      username,
      isLoggedIn,
      awaitingResponse,
      onPressLogout,
      onPressLogin,
      tangoHost,
    } = this.props;

    const version = `${process.env.REACT_APP_VERSION}`;

    return awaitingResponse ? null : (
      <div className="LogInOut">
        {isLoggedIn ? (
          <WhenLoggedIn
            username={username}
            onPressLogout={onPressLogout}
            version={version}
            tangoHost={tangoHost}
          />
        ) : (
          <WhenLoggedOut
            onPressLogin={onPressLogin}
            version={version}
            tangoHost={tangoHost}
          />
        )}
      </div>
    );
  }
}

interface StateProps {
  isLoggedIn: boolean;
  username?: string;
  awaitingResponse: boolean;
  loginFailure: boolean;
  tangoHost: string;
}

interface DispatchProps {
  onPressLogin: () => void;
  onPressLogout: () => void;
}

function mapStateToProps(state): StateProps {
  return {
    isLoggedIn: getIsLoggedIn(state),
    username: getUsername(state),
    awaitingResponse: getAwaitingResponse(state),
    loginFailure: getLoginFailure(state),
    tangoHost: getTangoHost(state),
  };
}

function mapDispatchToProps(dispatch): DispatchProps {
  return {
    onPressLogin: () => dispatch(openLoginDialog()),
    onPressLogout: () => dispatch(logout()),
  };
}

export default connect<StateProps, DispatchProps>(
  mapStateToProps,
  mapDispatchToProps
)(LogInOut);
