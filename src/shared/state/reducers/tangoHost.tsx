import {
  FETCH_TANGO_HOST_SUCCESS,
  FETCH_TANGO_HOST_FAILED,
} from '../actions/actionTypes';

const initialState: ITangoHostState = {
  data: '',
};

export interface ITangoHostState {
  data: string;}

export default function tangoHost(
  state: ITangoHostState = initialState,
  action: { type: any; data: any; errors: any }
): ITangoHostState {
  switch (action.type) {
    case FETCH_TANGO_HOST_SUCCESS:
      return {
        ...state,
        data: action.data.tangoHost,
      };
    case FETCH_TANGO_HOST_FAILED:
      return {
        ...state,
      };
    default:
      return state;
  }
}