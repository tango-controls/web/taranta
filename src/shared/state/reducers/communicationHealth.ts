import { FETCH_COMMUNICATION_HEALTH_FAILED, FETCH_COMMUNICATION_HEALTH_SUCCESS } from "../actions/actionTypes";
import { SubscribedAttr, Query, Mutation } from "../types/types";

const initialState: ICommunicationHealthState = {
  metrics: {
    executionTime: {
      query: [],
      mutation: []
    },
    subscribedAttrs: [],
    pubSub: true
  },
  error: ''
};

export interface ICommunicationHealthState {
  metrics: {
    executionTime: {      
      query: Query[],
      mutation: Mutation[]},
    subscribedAttrs: SubscribedAttr[],
    pubSub: boolean,
  },
  error: string
}

export default function communicationHealth(
  state: ICommunicationHealthState = initialState,
  action: { type: any; data: any; errors: any }
): ICommunicationHealthState {
  switch (action.type) {
    case FETCH_COMMUNICATION_HEALTH_SUCCESS:
      return {
        ...state,
        error: '',
        metrics: action.data.metrics,
      };
    case FETCH_COMMUNICATION_HEALTH_FAILED:
      return {
        ...state,
        error: action.data,
      };
    default:
      return state;
  }
}
