import { EXECUTE_COMMAND_FAILED, EXECUTE_COMMAND_SUCCESS, TOGGLE_MODE } from "../actions/actionTypes"
import commandOutput from "./commandOutput"

describe("commandOutput reducer tests", () => {
  it("TOGGLE_MODE & default mode test", () => {
    const state = {
      'sys/tg_test/1': {
        DevString: 'fdasfd'
      }
    }
    const action = {
      type: TOGGLE_MODE,
    }
    // @ts-ignore
    expect(commandOutput(state, action)).toEqual({});
    // @ts-ignore
    expect(commandOutput(state, {type: "RANDOM_TYPE"})).toEqual(state);
  })

  it("EXECUTE_COMMAND_SUCCESS & failed test", () => {
    const expectedOutput = {
      'sys/tg_test/1': {
        DevString: 'start'
      }
    }
    const action = {
      type: EXECUTE_COMMAND_SUCCESS,
      tangoDB: "testdb",
      command: "DevString",
      result: "start",
      device: "sys/tg_test/1"
    }

    // @ts-ignore
    expect(commandOutput({}, action)).toEqual(expectedOutput);
    const failedAction = {
      type: EXECUTE_COMMAND_FAILED,
      tangoDB: "testdb",
      command: "DevString",
      message: "Failed to execute cmd",
      device: "sys/tg_test/1"
    }
    const failedOutput = {
      "sys/tg_test/1": {
        DevString: "Failed to execute cmd"
      }
    }
    // @ts-ignore
    expect(commandOutput({}, failedAction)).toEqual(failedOutput);
  })
})