jest.mock('@apollo/client/core', () => ({
    ApolloLink: {
        from: jest.fn(() => ({})),
    },
}));

jest.mock('@apollo/client/link/ws', () => {
    return {
        WebSocketLink: jest.fn(() => ({
            request: () => { },
        })),
    };
});

jest.mock('@apollo/client', () => {
    const actualApollo = jest.requireActual('@apollo/client');
    return {
        ...actualApollo,
        ApolloClient: jest.fn(() => ({
            subscribe: () => ({
                subscribe: () => ({
                    unsubscribe: () => { },
                }),
            }),
            stop: () => { },
        })),
        gql: jest.fn((query) => query),
        InMemoryCache: jest.fn(() => ({})),
    };
});

jest.mock('@apollo/client/link/error', () => ({
    onError: jest.fn(() => ({
        request: () => { },
    })),
}));