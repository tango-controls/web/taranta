import React from "react";
import { Provider } from "react-redux";
import { createStore } from "redux";
import { render, cleanup } from "@testing-library/react";
import { QualityIndicator } from "./QualityIndicator";
import '@testing-library/jest-dom';

afterEach(cleanup);

const initialState = {
  messages: {
    tg_test: {
      attributes: {
        current: {
          quality: ["ATTR_VALID"]
        }
      }
    }
  }
};
const store = createStore(() => initialState);

describe("QualityIndicator", () => {
  it("should render with the correct quality", () => {
    const { getByText } = render(
      <Provider store={store}>
        <QualityIndicator name="current" deviceName="tg_test" />
      </Provider>
    );
    
    expect(getByText("VALID").closest("span")).toHaveClass("QualityIndicator");
    expect(getByText("VALID").closest("span")).toHaveAttribute("title", "ATTR_VALID");

  });
});
