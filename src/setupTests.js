import util from 'util';

Object.defineProperty(global, 'TextEncoder', {
  value: util.TextEncoder,
});

window.config = {
    "_comment": "This is a mock config file just for test purposes",
    "basename": "",
    "MIN_WIDGET_SIZE": 20,
    "WIDGETS_TO_HIDE": [],
    "WARN_WIDGETS": [],
    "WARN_WIDGETS_MSG": "Below widgets are not refactored but will be refactored shortly.",
    "SHOW_COMMAND_FILE_ON_DEVICES": true,
    "LOG_LEVELS": [
        "info",
        "debug",
        "warning",
        "error"
    ],
    "REDUX_LOG": false,
    "DEPRECATED": "This widget is deprecated and will be removed on version 1.4.0\nPlease use command widget instead",
    "ELASTIC_ACCEPTED_URLS": [
        "https://k8s.stfc.skao.int"
    ],
    "dateFormat": "YYYY-MM-DD",
    "defaultAttribute": [
        "state",
        "adminmode",
        "healthstate"
    ],
    "timeFormat": "hh:mm:ss.sssZ",
    "historyLimit": 100,
    "TANGO_DATABASES": [],
    "BIG_WIDGET": [
        "ATTRIBUTE_LOGGER",
        "ATTRIBUTE_PLOT",
        "ATTRIBUTE_SCATTER",
        "ATTRIBUTEHEATMAP",
        "BOX",
        "EMBED_PAGE",
        "IMAGEDISPLAY",
        "SPECTRUM",
        "SPECTRUM_2D",
        "TABULAR_VIEW",
        "TIMELINE"
    ]
};