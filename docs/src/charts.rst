Taranta charts
**************

Taranta has a folder called charts this, includes all the necessary templates to deploy the fully working Taranta suite software


Example deployment:
===================

The SKA organization maintains a working version of the Taranta suite software public available for all at: 

https://k8s.stfc.skao.int/webjive-namespace/taranta/devices *

* (The cluster is scheduled for a maintenance update and it will be unavailable for a couple of days)

All the images used to deploy this k8s cluster can be downloaded at:

https://artefact.skatelescope.org/

If one is interested in seeing how all this deploys one can take a look at:

https://gitlab.com/ska-telescope/skampi

(A couple of other charts containing more tango devices and server are deployed together, this is not mandatory to use Taranta) 
