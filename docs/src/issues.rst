Current known issues
====================


UTF-8 Encoding in POGO
----------------------

POGO is a graphical user interface tool used in the Tango Controls system for device class code generation.
It supports generating code in various programming languages, including C++, Python and JAVA.
POGO expects all input text (such as labels, descriptions, units, etc.) to be in UTF-8 encoding, 
which is the dominant character encoding for the web. Non-UTF-8 characters can cause issues during code 
generation and when interfacing with other Tango Controls components like tangoGQL and taranta.

Problem Description
-------------------

When a user inputs non-UTF-8 characters into POGO fields and generates code, these characters can cause 
encoding errors. For instance, C++ and Python generated code might not compile or run correctly, resulting 
in tangoGQL and taranta setting those fields to null values. Moreover, POGO itself can report errors such 
as (unicode error) 'utf-8' codec can't decode byte 0xb0 in position 0: invalid start byte.

|IMG1|

Resolution
----------

To prevent these issues, all textual input in POGO should be UTF-8 encoded. Below are steps to ensure UTF-8 compatibility:

- Use UTF-8 Compatible Editors:
  When entering textual information into POGO, ensure that you use an editor that supports UTF-8 encoding.

- Validate Input Text:
  Before pasting text into POGO, validate that it is UTF-8 encoded using online tools or text editors with encoding detection.

- Avoid Special Characters:
  If possible, avoid using special characters that do not translate well into UTF-8, such as certain proprietary or 
  system-specific symbols.

- Convert Existing Non-UTF-8 Text:
  If you have existing text that is not in UTF-8, use a conversion tool to transform the text into UTF-8 before inputting it into POGO.

- Error Handling:
  If you encounter an error related to UTF-8 encoding, review the text fields for non-UTF-8 characters and remove or convert them.

Taranta-Fix
-----------
If a user is using taranta version before 2.8.1 this issue will make so that the user can't use the device at all, however version
2.8.1 introduces a fix and makes the device available reporting only the current issue like one can see on the image below:

|IMG2|

Conclusion
----------

Adhering to UTF-8 encoding standards is essential for compatibility and functionality within the Tango Controls system. 
Following the above guidelines will help ensure smooth operation of the POGO tool and the systems that depend on its generated code.

For more information on UTF-8 encoding and how to manage it, refer to the following resources:

- [UTF-8 and Unicode Standards](https://www.unicode.org)
- [Tango Controls Documentation](https://tango-controls.readthedocs.io)

.. |IMG1| image:: _static/img/POGO-python-error.png
   :width: 650 px

.. |IMG2| image:: _static/img/POGO-taranta-result.png
   :width: 650 px
