Working with SVG
================

Basic usage of SVG
------------------

.. toctree::
    :maxdepth: 1

    svg_inkscape
    svg_link_device_to_svg
    svg_anchor_link
    svg_widget
    svg_custom_css


Advance usage of SVG
--------------------

.. toctree::
    :maxdepth: 1

    svg_format
    svg_rules
    svg_zoom
    svg_validation


Example SVG file 
----------------

.. toctree::
    :maxdepth: 1

    sampleSVG
