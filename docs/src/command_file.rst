Command File Widget
******************************************************************************************************************************************

This widget enables the dashboard designer to 
configure a command so that it sends the content of a file as an argument for 
a Tango command of a device. 

When running the dashboard, the user has to upload the file (max size 24MB) whose content  will be sent to the command. After the file is successfully uploaded, a link with its name is shown next to upload button; the user can then activate it to preview the content of uploaded file,
to change it on the fly, and to send it.

The designer can customize the widget look and feel by using images and CSS.






Widget settings
==============================================

The dashboard designer has the ability to customize these elements:

* Title of widget
* Upload button label
* Send button label
* Device and command
* Whether to show device name  and command name on send button
* Whether to require a confirmation
* Whether to display the output of the command
* Whether to align the send button to right of the parent widget or not
* Text and background color
* Font size and family
* The CSS to apply to outer parent widget div
* The CSS to apply to the upload button
* The  CSS to apply to the send button

\ |IMG1|\ 

Any web image (or URL) can be used to customize the upload and send button. In addition,
CSS rules can be applied to the widget; for example

.. code-block:: css

   color: green;
   background-color: red;

.. note::

   For a command to be listed when configuring the widget, it must accept parameters.



Widget look and feel
==============================================

The following pictures show how the widget looks like in a running dashboard.
User can drag/move "file content popup" to view content of main/dashboard screen.

\ |IMG2|\ 

When the user clicks the "Send" button (the text of the button can be configured), a confirmation popup appears to confirm the action, which fires command with uploaded file as argument to the device.
The output of the command this can be seen next to the button.

\ |IMG3|\ 

On-the-fly changes of the file content 
==============================================

When the user previews the content of the uploaded file, it is possible to modify it on-the-fly. These changes are applied only to the uploaded content: Taranta does not change the file that was uploaded.

\ |IMG4|\ 

Taranta keeps the original file content and it is possible to restore it, by clicking on the "Restore" button. It is also possible to compare the original content with the updated one, by clicking on the "Compare" button.  This shows a "diff" of the modified contents against the original one that was uploaded. 

The "Edit" button can be used to make other changes. 

\ |IMG5|\ 


When the original value is modified,  the filename label in the widget changes, and the text [MODIFIED] is shown as a prefix of the label. In addition, the confirmation pop-up informs the user that the file content was modified. 

\ |IMG6|\ 

\ |IMG7|\ 

User needs to be logged in to use this widget
=============================================
\ |IMG8|\ 



.. bottom of content

.. |IMG1| image:: _static/img/command_file_inspector.png
   :width: 289 px

.. |IMG2| image:: _static/img/command_file_output.png
   :height: 350 px
   :width: 564 px

.. |IMG3| image:: _static/img/command_file_confirm.png
   :width: 546 px

.. |IMG4| image:: _static/img/command_file_edit_mode.png
   :width: 350 px

.. |IMG5| image:: _static/img/command_file_compare_mode.png
   :width: 350 px

.. |IMG6| image:: _static/img/command_file_label_modified.png
   :width: 350 px

.. |IMG7| image:: _static/img/command_file_confirmation_modified.png
   :width: 350 px

.. |IMG8| image:: _static/img/CommandFileLogin.png
   :width: 415 px


