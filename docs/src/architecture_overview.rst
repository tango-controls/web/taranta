
Taranta Architecture
####################

Taranta is composed of four projects: 

#. **Taranta Frontend**  https://gitlab.com/tango-controls/web/taranta

#. **Taranta Dashboards**  https://gitlab.com/tango-controls/web/taranta-dashboard

#. **Taranta Auth** https://gitlab.com/tango-controls/web/taranta-auth

#. **TangoGQL**  https://gitlab.com/tango-controls/web/tangogql


The following diagram shows an overview of the architecture: 

.. figure:: _static/img/Taranta_Architecture.png
   :width: 100%
   :alt: Block diagram of the architecture


Taranta Frontend is a React_  client that is used both to browse, inspect and control Tango devices and to create and run dashboards, each composed of  widgets. Taranta Frontend accesses the Tango Control Framework through **TangoGql**, a GraphQL_ interface to Tango. The communication between Taranta and TangoGQL is managed by an appropriate frontend component. 

.. _React: https://reactjs.org/
.. _GraphQL: https://graphql.org/

To use Taranta, an anonymous user can run dashboards and can browse and inspect devices. However, to be able to send commands to devices, to change their attributes and to create or modify dashboards, you need to authenticate with Taranta. Taranta uses **taranta-auth**  to manage users; it  accesses an LDAP repository or a  JSON file to retrieve user information. 

The dashboards created by an authenticated user are stored in a MongoDB_  database through the **dashboard-repo**  application. 

.. _MongoDB: https://www.mongodb.com/



