Dashboard environment
*********************

Taranta offers a flexible environment configuration, allowing dashboards to be associated with specific URLs. This feature is particularly useful when operating with Kubernetes (k8s) namespaces, enabling users to bind a dashboard to a unique environment. The environment settings for each dashboard are managed through the dashboard's configuration panel under the "Environment" tab.

1. Environment Configuration (string array)
===========================================
This array holds the URL environments where the dashboard will be accessible. The environments are stored as an array of strings in the MongoDB database, and users can add, edit, or remove entries through the dashboard configuration interface.

- An environment entry such as ``http://localhost:3000/testdb`` will restrict the dashboard to be accessible only when the application is accessed through this specific URL.
  
- The wildcard entry ``*/*`` serves as a universal matcher, allowing the dashboard to be accessible from any environment.

- Multiple environments can be specified for a single dashboard, such as ``http://localhost:3000/testdb`` and ``http://localhost:3001/testdb``, making the dashboard available on both URLs.

- To associate a dashboard with a specific k8s namespace URL, such as ``https://k8s.stfc.skao.int/taranta-namespace/taranta``, the exact URL should be added to the environment configuration. The dashboard will then only be available at that particular URL.

Examples:
A dashboard configured with the environment ``http://localhost:3000/testdb`` will only appear when the Taranta application is accessed via ``http://localhost:3000/testdb``.

A dashboard configured with the environment ``*/*`` will be universally accessible, regardless of the URL used to access the Taranta application.

A dashboard with multiple environments specified, such as ``http://localhost:3000/testdb`` and ``http://localhost:3001/testdb``, will be available when accessed through either of the listed URLs.

2. Applying Configuration Changes
=================================
To apply changes to the environment configuration:

- Navigate to the "Environment" tab in the dashboard configuration panel.
  
- Use the input field to add a new environment URL or select an existing one to edit or remove.

- Click "Save" to persist the changes. The update will reflect immediately, subject to the access URL of the Taranta application.

Below is an illustrative representation of environment configurations:

    - A dashboard with a wildcard environment allowing access from any URL.

   \ |AllEnvironments|

    - A dashboard restricted to a single specified environment.

    \ |SpecificEnvironment|

    - A dashboard accessible from multiple specified environments.

    \ |MultipleEnvironments|

.. |AllEnvironments| image:: _static/img/All.png

.. |SpecificEnvironment| image:: _static/img/3000.png
   
.. |MultipleEnvironments| image:: _static/img/3000+3001.png

.. bottom of content

